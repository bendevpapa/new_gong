-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 26, 2020 at 07:52 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gong_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) NOT NULL,
  `competition_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `competition_id`, `user_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 5, 14, 'dsaa', '2020-06-26 10:23:13', '2020-06-26 10:23:13'),
(2, 5, 14, 'dsaa', '2020-06-26 10:24:00', '2020-06-26 10:24:00'),
(3, 5, 14, 'sadasd', '2020-06-26 10:24:17', '2020-06-26 10:24:17'),
(4, 5, 14, 'dsdsfds', '2020-06-26 10:24:42', '2020-06-26 10:24:42'),
(5, 5, 14, 'dsdsfds', '2020-06-26 10:26:04', '2020-06-26 10:26:04'),
(6, 5, 14, 'asdasd', '2020-06-26 10:26:16', '2020-06-26 10:26:16'),
(7, 5, 14, 'adasd', '2020-06-26 10:29:31', '2020-06-26 10:29:31'),
(8, 5, 14, 'asd', '2020-06-26 10:34:24', '2020-06-26 10:34:24'),
(9, 5, 14, 'asdsadasd', '2020-06-26 10:34:29', '2020-06-26 10:34:29'),
(10, 5, 14, 'asdsadasd', '2020-06-26 10:34:34', '2020-06-26 10:34:34'),
(11, 5, 14, 'testing', '2020-06-26 10:37:20', '2020-06-26 10:37:20'),
(12, 5, 14, 'testingqwe', '2020-06-26 10:37:26', '2020-06-26 10:37:26'),
(13, 5, 14, 'testingqwe', '2020-06-26 10:37:29', '2020-06-26 10:37:29'),
(14, 5, 14, 'testingqwe', '2020-06-26 10:37:30', '2020-06-26 10:37:30'),
(15, 5, 14, 'testingqwe', '2020-06-26 10:37:33', '2020-06-26 10:37:33'),
(16, 5, 14, 'testingqwe', '2020-06-26 10:37:34', '2020-06-26 10:37:34'),
(17, 5, 14, 'testingqwe', '2020-06-26 10:37:34', '2020-06-26 10:37:34'),
(18, 5, 14, 'testingqwe', '2020-06-26 10:37:34', '2020-06-26 10:37:34'),
(19, 5, 14, 'testingqwe', '2020-06-26 10:37:35', '2020-06-26 10:37:35'),
(20, 5, 14, 'asd', '2020-06-26 10:38:21', '2020-06-26 10:38:21'),
(21, 5, 14, 'test', '2020-06-26 10:40:38', '2020-06-26 10:40:38'),
(22, 5, 14, 'ewrwer', '2020-06-26 10:42:35', '2020-06-26 10:42:35'),
(23, 5, 14, 'asd', '2020-06-26 10:45:00', '2020-06-26 10:45:00'),
(24, 5, 14, 'asd', '2020-06-26 10:46:23', '2020-06-26 10:46:23'),
(25, 5, 14, 'erert', '2020-06-26 10:50:14', '2020-06-26 10:50:14'),
(26, 5, 14, 'erert', '2020-06-26 10:50:22', '2020-06-26 10:50:22'),
(27, 5, 14, 'erertasd', '2020-06-26 10:50:28', '2020-06-26 10:50:28'),
(28, 5, 14, 'asd', '2020-06-26 11:14:28', '2020-06-26 11:14:28'),
(29, 5, 14, 'fahad', '2020-06-26 12:15:46', '2020-06-26 12:15:46'),
(30, 5, 14, 'tt', '2020-06-26 12:23:30', '2020-06-26 12:23:30'),
(31, 5, 14, 'ttt', '2020-06-26 12:26:53', '2020-06-26 12:26:53'),
(32, 5, 15, 'rrr', '2020-06-26 12:36:22', '2020-06-26 12:36:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('regions')->delete();
        
        \DB::table('regions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'country_id' => 168,
                'region_name' => 'Lahore',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
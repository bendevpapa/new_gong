<?php

use Illuminate\Database\Seeder;

class UserDetailsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_details')->delete();
        
        \DB::table('user_details')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'name' => 'Hassan Raza',
                'username' => 'hassan',
                'date_of_birth' => '1995-10-03',
                'gender' => 'male',
                'profile_photo' => 'hassan1584078056.png',
                'region_id' => 1,
                'country_id' => 168,
                'created_at' => '2020-03-13 05:40:56',
                'updated_at' => '2020-03-13 05:40:56',
            ),
        ));
        
        
    }
}
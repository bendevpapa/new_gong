<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 2,
                'tier_id' => 1,
                'email' => 'abc@mail.com',
                'phone' => '03314173882',
                'email_verified_at' => NULL,
                'password' => '$2y$10$7vCeEdnDtKhZu0UdevwYle6wLtW8f/iPXCytAKN5z3XMfNgi1L/BW',
                'remember_token' => NULL,
                'created_at' => '2020-03-13 05:40:38',
                'updated_at' => '2020-03-13 05:40:38',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tiers')->delete();

        DB::table('tiers')->insert([
            0 => [
                'tier_name' => 'Premier',
                'tier_level' => 0
            ],
            1 => [
                'tier_name' => 'Competition Winner',
                'tier_level' => 1
            ],
            2 => [
                'tier_name' => 'Popular',
                'tier_level' => 2
            ],
            3 => [
                'tier_name' => 'Rising Star',
                'tier_level' => 2
            ],
            4 => [
                'tier_name' => 'Got Talent',
                'tier_level' => 4
            ],
            5 => [
                'tier_name' => 'Professional',
                'tier_level' => 5
            ],
            6 => [
                'tier_name' => 'Serious Talent',
                'tier_level' => 6
            ],
            7 => [
                'tier_name' => 'Potential Star',
                'tier_level' => 7
            ],
            8 => [
                'tier_name' => 'Star',
                'tier_level' => 8
            ],
            9 => [
                'tier_name' => 'Superstar',
                'tier_level' => 9
            ],
        ]);

    }
}

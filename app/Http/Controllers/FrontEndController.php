<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modals\Country;
use App\Modals\Region;

class FrontEndController extends Controller
{
    public function privacy(){
    	$countries = Country::orderBy('name','asc')->get();
        $regions = Region::all();
        $location = ip_info(get_client_ip());
    	return view('privacy',compact('countries','regions','location'));
    }
    public function termOfCondition(){
    	$countries = Country::orderBy('name','asc')->get();
        $regions = Region::all();
        $location = ip_info(get_client_ip());
    	return view('term-and-condition',compact('countries','regions','location'));
    }
}

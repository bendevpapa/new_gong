<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Modals\Competition;
use App\Modals\UserContent;
use App\Modals\UserTier;
use App\Utils\GoogleCloudRequest;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Validator;

class UserCompeteController extends Controller
{
    public function addContentForPublicCompetition(Request $request)
    {
        try {
            $user = Auth::user()->id;

        } catch (Exception $exception) {
            return response()->json(['status' => 404, 'Error' => "Please Login To Compete"]);
        }
        set_time_limit(200);
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'file_type' => 'required',
            'file' => 'required',
            'category_id' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = [];
            $errors['status'] = 422;
            $errors['message'] = 'Validation Error';
            $errors['errors'] = $validator->messages();
            return response()->json([
                'status' => $errors['status'],
                'Message' => $errors['message'],
                'error' => $errors['errors'],
                
            ]);

        }

        $data = [];
        $file = $request->file('file');

        $bucketName = config('constant.GCS_BUCKET');
        $fileContent = file_get_contents($file->getRealPath());
        if ($request->file_type == 'video') {
            $image = $request->img_thum; // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = rand(111452, 15465460) . '.' . 'png';
        }
        $cloudPath = 'uploads/' . $request->file_type . '/' . time() . $file->getClientOriginalName();

        $googleRequest = new GoogleCloudRequest();

        if ($request->file_type == 'video') {

            $img_path = 'uploads/' . 'img' . '/' . time() . $imageName;

            $googleRequest->uploadFile($bucketName, base64_decode($image), $img_path);

        }

        $isSucceed = $googleRequest->uploadFile($bucketName, $fileContent, $cloudPath);

        if ($isSucceed) {
            $data['status'] = true;
            $data['message'] = 'content uploaded successfully';
            $fileInfo = $googleRequest->getFileInfo($bucketName, $cloudPath);
            $user_tier = UserTier::where('user_id', $user)
                ->where('category_id', $request->category_id)
                ->first();

            $userContent = new UserContent();
            $userContent->user_tier_id = $user_tier->id;
            $userContent->file_type = $request->file_type;
            $userContent->file_gcs_self_link = $fileInfo['selfLink'];
            $userContent->file = $cloudPath;
            if ($request->file_type == 'video') {
                $userContent->thumbnail = $img_path;
            }
            $userContent->category_id = $request->category_id;
            $userContent->name = $request->name;
            $userContent->genre_id = $request->genre_id;
            $userContent->save();

            $user_contenet = UserContent::where('category_id', $request->category_id)
                ->where('is_competition', 0)
                ->where('file_type', $request->file_type)
                ->whereHas('userTier', function ($qry) use ($user_tier) {
                    $qry->where('category_id', request()->category_id)
                        ->where('is_competition', 0)
                        ->where('taxonomie_id', $user_tier->taxonomie_id);

                })
                ->orderBy('created_at', 'desc')
                ->limit(4)
                ->get();

            if (count($user_contenet) >= 4) {

                $supplier_email = getVal('email', 'users', 'id', $input['user_id']);
                $subject = "Testing";
                $content = "<h2>invoice email testing</h2>";
                $html_body = html_entity_decode($content);
                $fromName = "ERP-POS System";
                sendMail($supplier_email, $fromName, $sender_email = 'erppos1122@gmail.com', $subject, $html_body);

                $u_tier = UserTier::where('user_id', $user)
                    ->where('category_id', $request->category_id)
                    ->first();
                $competititon = new Competition();
                $competititon->competition_type = 'public';
                $competititon->competition_media = $request->file_type;
                $competititon->competition_user_level_id = $u_tier->id;
                $competititon->category_id = $u_tier->category_id;
                $competititon->start_at = date('Y-m-d H:i:s');
                $competititon->end_at = date('Y-m-d H:i:s', strtotime(' +10 minutes'));

                if ($request->file_type == 'video') {
                    $competititon->priority = 3;
                } elseif ($request->file_type == 'audio') {
                    $competititon->priority = 2;
                } elseif ($request->file_type == 'image') {
                    $competititon->priority = 1;
                }
                $is_saved = $competititon->save();

                if ($is_saved) {
                    foreach ($user_contenet as $key => $value) {
                        UserTier::where('id', $value->userTier->id)->update(['is_competition' => 1]);
                        $competititon_user = new CompetitionUser();
                        $competititon_user->user_id = $user;
                        $competititon_user->user_content_id = $value->id;
                        $competititon_user->competition_id = $competititon->id;
                        $is_c_saved = $competititon_user->save();
                        if ($is_c_saved) {
                            $user_contenet = UserContent::where('id', $value->id)->update(['is_competition' => 1]);
                        }
                    }
                }
            }

            $data['data'] = [
                'file_link' => config('constant.GCS_BASE_URL') . config('constant.GCS_BUCKET') . '/' . $cloudPath,
                'content_id' => $userContent->id,
            ];
        } else {
            $data['status'] = false;
            $data['message'] = 'There is problem uploading content';
            $data['data'] = [];
        }
        return response()->json(['type' => 'Public', 'result' => $data]);
    }
    public function addContentForPrivateCompetition(Request $request)
    {
        try {
            $user = Auth::user()->id;

        } catch (Exception $exception) {
            return response()->json(['status' => 404, 'Error' => "Please Login To Compete"]);
        }

        $competition = Competition::where('comopetition_by_type', 'custom')
            ->where('is_active', 3)
            ->where('competition_type', 'private')
            ->where('competition_media', $request->type)
            ->where('category_id', $request->category_id)
            ->where('genre_id', $request->genre_id)
            ->whereHas('elegible', function ($qry) use ($user, $request) {
                $qry->where('user_id', $user)
                    ->where('category_id', $request->category_id);
            })->paginate(4);

        if ($competition) {
            return response()->json(['status' => 200, 'Type' => 'Private', 'competition' => $competition]);
        }
        return response()->json(['status' => 404, 'message' => 'Not Found']);

    }
}

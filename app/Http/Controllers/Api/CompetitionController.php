<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Modals\Comment;
use App\Http\Requests\CompetitionCreate;
use App\Modals\Competition;
use App\Modals\CompetitorSet;
use App\Modals\Taxonomie;
use App\Modals\UserDetail;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompetitionController extends Controller
{
    // Competition API
    public function getCompetionCreate(CompetitionCreate $request)
    {
        try {
            $competition_by = Auth::user()->id;

        } catch (Exception $exception) {
            return response()->json(['status' => 404, 'Error' => "Please Login To Create Competition"]);
        }

        $validator = $request->validated();


        if ($validator->fails()) {
            return response()->json(['status' => 422, 'Message' => 'Validation', 'error' => $validator->errors()]);
        }
        $code = '';
        $competitors_s = CompetitorSet::where('id', request()->no_of_competitior)->first();

        if ($competitors_s) {
            $competition = new Competition();
            $competition->competition_name = request()->competition_name;
            $competition->competition_end_type = request()->create_competition_type;
            $competition->category_id = request()->create_category_id;
            $competition->parent_id = 0;
            $competition->genre_id = request()->create_genre_id;
            $competition->competition_media = request()->file_type;
            $competition->no_of_competitiors = request()->no_of_competitior;
            $competition->no_of_rounds = request()->no_of_round;
            $competition->competitior_set_id = $competitors_s->id;
            $competition->voting_interval = request()->voting_time;
            $competition->duration = request()->competition_duration;
            $competition->competition_type = request()->access;
            $competition->competition_set = request()->competition_set;
            $competition->eligible_tier_id = request()->eligible_tier;
            $competition->competition_user_level_id = request()->eligible_tier;
            $competition->country_id = request()->countries;
            $competition->region_id = request()->region;
            $competition->comopetition_by_type = 'custom';
            $competition->competition_by = $competition_by;
            $competition->is_active = 4;
            $competition->save();
            $no_round = $competitors_s->no_of_rounds; // 3
            $round = json_decode($competitors_s->round_details);
            $number_of_competitor_each = 0;
            $number_of_competition = 0;
            if (request()->access == 'private') {
                $code = rand(1021145, 2546498);
            }
            return response()->json([
                "joining_code" => $code,
                'status' => 200,
                "message" => "Competition Created Successfully",
                "success" => true,
            ]);

        } else {
            return response()->json([
                'status' => 404,
                "message" => "competitors are not defined",
                "Error" => true,
            ]);

        }

    }

    //competition Voting API
    public function competitionUserJudge(Request $request)
    {
        try {
            $user = Auth::user()->id;
        } catch (Exception $exception) {
            return response()->json(['status' => 404, 'Error' => "Please Login To Create Competition"]);
        }

        $validator = Validator::make($request->all(), [
            'crown_' => 'required',
            'competition_user_id' => 'required',
            'competition_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 422, 'Message' => 'Validation', 'error' => $validator->errors()]);
        }

        $crown = $request->crown_;
        $c_id = $request->competition_user_id;
        $com = CompetitionUserJudge::where('user_judge_id', $user)
            ->where('competition_id', $request->competition_id)->first();
        if ($com) {
            $error = 'Already you have give the voting';
            return response()->json(['status' => 409, 'Error' => $error]);
        }
        foreach ($crown as $key => $value) {
            if (empty($value)) {
                $error = 'All Field is required';
                return response()->json(['status' => 400, 'Error' => $error]);
            }
        }
        $com = CompetitionUserJudge::where('user_judge_id', $user)
            ->where('competition_id', $request->competition_id)->first();
        if ($com) {
            $error = 'Already you have give the voting';
            return response()->json(['status' => 409, 'Error' => $error]);

        } else {
            foreach ($crown as $key => $value) {
                $competition_judge = new CompetitionUserJudge();
                $competition_judge->competition_user_id = $request->competition_user_id[$key];
                $competition_judge->competition_id = $request->competition_id;
                $competition_judge->user_judge_id = $user;
                $competition_judge->expression = $value;
                $competition_judge->save();
            }
            $success = 'You have vote successfully';
            return response()->json(['status' => 200, 'success' => $success]);

        }
    }
    public function getSingleCompetitionDetails(Request $request)
    {
        // checking if user login
        try {
            $user = Auth::user()->id;
        } catch (Exception $exception) {
            return response()->json(['status' => 404, 'Error' => 'Please Login to View Competition Details']);
        }

        if (empty($request->competition_id)) {
            $error = "Competition is not Found";
            return response()->json(['status' => 404, 'Error' => $error]);
        }
        $competition = Competition::where(['competition_by' => $user, 'id' => $request->competition_id])->first();
        $data = new Carbon($competition->created_at);
        $date = $data->toDateString();
        $tier = Taxonomie::select("name")->where("id", $competition->eligible_tier_id)->first();

        $details = [
            "name" => $competition->competition_name,
            "duration" => $competition->duration,
            "number_of_competitors" => $competition->no_of_competitiors,
            "category" => $competition->tier->name,
            "sub_category" => $competition->genre->name,
            "Tier" => $tier->name,
            "competitor_name" => 'Talent',
            "competitor_profile_image" => 'https: //storage.googleapis.com/gong_bucket/uploads//159248986433151461929png',
            "date" => $date,

        ];
        return response()->json(['status' => 200, 'result' => $details]);
    }
    public function getCompetitionComments()
    {

        $details = UserDetail::join('comments', 'user_details.user_id', '=', 'comments.user_id')
            ->select("user_details.username", "user_details.profile_photo", "comments.content")
            ->get();
        $totalComents = Comment::all();
        return response()->json(['status' => 200, 'total' => count($totalComents), 'details' => $details]);
    }
    public function getCompetitionCommentPost(Request $request)
    {
        try {

            $currentUser = Auth::user()->id;

        } catch (Exception $exception) {
            return response()->json(['status' => 404, 'Error' => "Please Login To Create Competition"]);
        }
        $userDetails = UserDetail::select("username", "profile_photo")->where("user_id", $currentUser)->first();

        $comment = new Comment();
        $comment->competition_id = request()->competition_id;
        $comment->user_id = $currentUser->id;
        $comment->content = request()->comment;
        $comment->save();

        $last_insert_id = $comment->id;
        if ($last_insert_id > 0) {
            $totalComents = Comment::all();
            return response()->json(['status' => 200, 'message' => 'Posted', 'comment' => request()->comment,
                'total' => count($totalComents), 'userDetails' => $userDetails->toArray()]);
        } else {
            return response()->json(['status' => 404, 'message' => 'invalid', 'comment' => '', 'total' => 0, 'userDetails' => '']);
        }

    }
    public function getShareableLink()
    {
        $key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';
        $url = URL::previous();
        $lastParam = explode("?q=", $url);
        $id = end($lastParam);
        $lastParam = encryptString($id, $key);
        $sharedUrl = url('/') . "/voting?q=" . $lastParam;

        $counters = User_sharedUrlCount::all();

        return response()->json(['sharedUrl' => $sharedUrl]);
    }

}

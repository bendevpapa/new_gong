<?php

namespace App\Http\Controllers;

use App\Modals\User_following;
use App\Modals\UserDetail;
use App\Modals\User_follower;
use Illuminate\Http\Request;

class UserFollowController extends Controller
{
    public function index()
    {

        $username = request()->searched_username;
        $photo = UserDetail::select("id", "profile_photo")->where("username", $username)->first();

        return view('frontend.users.pages.profile.index', compact('username', 'photo'));
    }

    public function followUser(Request $request)
    {
        $user_id = $request->user_id; //The user who got Follow
        $follower_id = $request->follower_id; //The user who are following the other user
        $is_follow = $request->is_follow;

        $followers = '';
        $followings = '';
        $unfollow_user = '';

        $is_user_followed_already = User_follower::where(['user_id' => $user_id, 'follower_id' => $follower_id])->exists();
        if (!$is_user_followed_already) {

            $user_follow = new User_follower();
            $user_follow->user_id = $user_id;
            $user_follow->follower_id = $follower_id;
            $user_follow->is_follow = $is_follow;
            $user_follow->save();

            $last_inserted_id = $user_follow->id;
            $followers = User_follower::where(['is_follow' => 'yes', 'user_id' => $user_id])->count();
            $followings = User_follower::where(['is_follow' => 'yes', 'follower_id' => $user_id])->count();

            if ($last_inserted_id > 0)
                return response()->json(['status' => 1, 'message' => 'success', 'followers' => $followers, 'followings' => $followings]);
            else
                return response()->json(['status' => 0, 'message' => 'error']);
        } else {
            $message = '';
            $followed_user = User_follower::where(['user_id' => $user_id, 'follower_id' => $follower_id])->first();
            $followers = User_follower::where(['is_follow' => 'yes', 'user_id' => $user_id])->count();
            $followings = User_follower::where(['is_follow' => 'yes', 'follower_id' => $user_id])->count();

            if ($followed_user->is_follow == 'no') {
                $message = "follow";
                if($request->flag !== 'isUserAollowedAlready') {
                    User_follower::where(['user_id' => $user_id, 'follower_id' => $follower_id])->update(['is_follow' => 'yes']);
                }
                return response()->json(['status' => 1, 'message' => $message, 'followers' => $followers, 'followings' => $followings]);
            } else {
                $message = "unfollow";
                if($request->flag !== 'isUserAollowedAlready') {
                    User_follower::where(['user_id' => $user_id, 'follower_id' => $follower_id])->update(['is_follow' => 'no']);
                }
                return response()->json(['status' => 1, 'message' => $message, 'followers' => $followers, 'followings' => $followings]);
            }
        }


    }
}

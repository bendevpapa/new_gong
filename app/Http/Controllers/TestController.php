<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modals\User;
use App\Modals\UserContent;
use App\Modals\UserTier;  
use App\Modals\UserDetail; 
use App\Modals\Taxonomie;
use App\Modals\Competition;

use Google\Cloud\VideoIntelligence\V1\VideoIntelligenceServiceClient;
use Google\Cloud\VideoIntelligence\V1\Feature;
use Google\Cloud\VideoIntelligence\V1\Likelihood;
// use App\;

use App\Mail\MediaRejectMail;
use Illuminate\Support\Facades\Mail;


use Illuminate\Support\Facades\Hash;
class TestController extends Controller
{
    public function index(){
    	ini_set('max_execution_time', 180);
    	// $s = User::where('id','>',0)->get();
    	//  dd($s);
    	// User::role('user')->with('userTier')->delete();
    	// User::role('user')->with('userContents')->delete();
    	// // User::role('user')->removeRole();
    	// User::role('user')->delete();
    	// $user = User::role('user')->get();
    	for($i = 1; $i<=200; $i++){
	    	$taxonomie = Taxonomie::where('type','user_level')
	                                ->where('name','Premier')
	                                ->first();
	        // $request->merge(['taxonomie_id'=>$taxonomie->id]); 
	        $user = new User();
	        $user->email = 'a'.$i.'@gmail.com';
	        $user->phone = 12312312323;
	        $password = '123456'; // password is form field
			$hashed = Hash::make($password);
	        $user->password = 123456;
	        $user->taxonomie_id = 1;
	        $user->save();
	        if ($user){
	             
	                $user_tier = new UserTier();
	                $user_tier->category_id = 11;
	                $user_tier->taxonomie_id = 1;
	                $user_tier->user_id = $user->id;
	                $user_tier->save();

	                $user_tier = new UserTier();
	                $user_tier->category_id = 12;
	                $user_tier->taxonomie_id = 1;
	                $user_tier->user_id = $user->id;
	                $user_tier->save();

	                $user_tier = new UserTier();
	                $user_tier->category_id = 13;
	                $user_tier->taxonomie_id = 1;
	                $user_tier->user_id = $user->id;
	                $user_tier->save();

	                $user_tier = new UserTier();
	                $user_tier->category_id = 14;
	                $user_tier->taxonomie_id = 1;
	                $user_tier->user_id = $user->id;
	                $user_tier->save();

	                $user_tier = new UserTier();
	                $user_tier->category_id = 15;
	                $user_tier->taxonomie_id = 1;
	                $user_tier->user_id = $user->id;
	                $user_tier->save();

	                $user_tier = new UserTier();
	                $user_tier->category_id = 16;
	                $user_tier->taxonomie_id = 1;
	                $user_tier->user_id = $user->id;
	                $user_tier->save();

	                
	             
	            $user->assignRole('user');
	            $userDetail                 = new UserDetail();
		        $userDetail->user_id        = $user->id;
		        $userDetail->name           = 'abc'.rand(155,5645);
		        $userDetail->username       = 'abc'.rand(155455,555645);
		        $userDetail->date_of_birth  = '1996-06-06';
		        $userDetail->gender         = 'male';
		        $userDetail->profile_photo  = 'uploads//1585635988pexels-photo-814499.jpeg';
		        $userDetail->region_id      = 3176;
		        $userDetail->country_id     = 167;
		        $userDetail->save();
	    	}
		}
    }

    public function competitionDetailForTesting(){
    	$competition = Competition::paginate(10);
    	// dd($competition);
    	return view('testing',compact('competition'));
    }

    public function videoTest(){

    	$video = new VideoIntelligenceServiceClient([
		    'credentials' => json_decode(file_get_contents("C:\Users\mohsin baig\Downloads\livegong-bb13b1b01b23.json"),true)
		]);
		$options = [];
		# Execute a request.
		$operation = $video->annotateVideo([
			// https://storage.googleapis.com/gong_bucket/uploads/video/1587643263Sos_mi_Vida_adelanto_capitulo_55.mp4
		    'inputUri' => 'gs://gong_bucket/uploads/video/1587648984y2mate.com_-_Video_4mb_06YGoi6gNms_360p.mp4',
		    'features' => [Feature::EXPLICIT_CONTENT_DETECTION]
		]);

		# Wait for the request to complete.
		$operation->pollUntilComplete($options);

		# Print the result.
		if ($operation->operationSucceeded()) {
		    $results = $operation->getResult()->getAnnotationResults()[0];
		    $explicitAnnotation = $results->getExplicitAnnotation();
		    foreach ($explicitAnnotation->getFrames() as $frame) {
		        $time = $frame->getTimeOffset();
		        printf('At %ss:' . PHP_EOL, $time->getSeconds() + $time->getNanos()/1000000000.0);
		        printf('  pornography: ' . Likelihood::name($frame->getPornographyLikelihood()) . PHP_EOL);
		    }
		} else {
		    print_r($operation->getError());
		}
    }

    public function emailTest(){
    	Mail::to('developerjobesk@gmail.com')->send(new MediaRejectMail());
    	return redirect()->back();
    }
    public function phoneTest(){
    	sendVerificationMessage('+923180583512','Hi Mohsin How are youuuuuuuuuu');
    }
    public function pp(){
    	return view('pp');
    }
}

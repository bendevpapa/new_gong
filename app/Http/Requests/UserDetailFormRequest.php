<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserDetailFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'username' => 'required|unique:user_details,username|max:20',
            'date_of_birth' => ((request()->agent_role_hidden == 'agent') || (request()->spoonser_role_hidden == 'spoonser')) ? '' : 'required|date|before:-13 years',
            'gender' => 'required',
            'profile_photo' => 'required|mimes:jpeg,jpg,png',
            'region_id' => 'required',
            'country_id' => 'required',
            'date_of_birth.before' => 'You must be 13 years or Older'
        ];
    }
}

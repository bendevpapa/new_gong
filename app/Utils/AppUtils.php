<?php


namespace App\Utils;


class AppUtils{

    /**
     * categories array
     */
    public const CATEGORIES = ['vocals', 'dance', 'comedy', 'acting', 'cheering', 'modeling'];

    /**
     * accepted file type array
     */
    public const FILE_TYPES = ['audio', 'video', 'image'];
}

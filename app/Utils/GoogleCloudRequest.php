<?php


namespace App\Utils;


use Exception;
use Google\Cloud\Storage\StorageClient;

class GoogleCloudRequest{

    /**
     * @var string
     * private key generated from google console
     */
    private $privateKeyFileContent;

    /**
     * GoogleCloudRequest constructor.
     */
    public function __construct(){
        $this->privateKeyFileContent = '{
           "type": "service_account",
  "project_id": "gong-2",
  "private_key_id": "ba452c4bd6cc1f47a695efdcb581c01d3468b84a",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCVyPtWfMiqAOt0\nNMNJFczR6/xPRp6i/mBFcAC6b0ddOl0Rz4VgxkId8pWEYNP/Uxb2S2jYgW/nqLWc\ni3bJO19+YkvPK0WQ0ACbUNON4dV22sUy1rya7zoL4xS6q+ZoZgd/2uh97RcSLw4l\nFNBax0rF8MxT6FrAW5Un1f8WhlHGysCWctau2kkmmJKiQ1NLP+k3j38mx7yp6Yu0\nLD5BpjYVjs0GkFqWTNLOtvnDU14f6WqNNORuO8v1JZBC+2fdEkel5fhx9aLzIszv\nMN+SixWlzhik5EotFogdK6s5w6UbVB0/kiVjYA6UYiAVbJK6mitCdn0bnWai/LE+\n7z1xLd17AgMBAAECggEADyyA+GuEFo4o9tOWNFQJilCfoFZVTB98mDw2+Va8QjUJ\n6FEqFWTa84FiOpARRJc3HHSvkT4CR1PKMNWDZJ0SWPwolZBa0pydGx7j4kaOY0/Z\nVK5NWrLBIiLQiZ1QJk5ZFHo+6EGO/0VrZ5VFYD+P6HUy9eMe4nLEraLvsxP2D9nN\nsvrotop5Bg1Ew4/B0+JWRAciF4LLVvxI5Jf12aunnXOPvALXUzy9IWH1I5gZzhv4\nL4rJdI6+QSon0Et9aFuMN1FFKsL08IZN802RS+RvBBGIQvJK7XC/d6qNbrry5VYb\n45Ign2458K+f8hwpN1eN0sQRDfN2LAcz/VVJnSSB7QKBgQDGF5siY8VchZ1aUguO\n46hvhrbKaXj84g8O6nZNT/y9j874ExpQXip4wrb/gbhMaJxoqY1LkdNdBQH5JSRr\nLyGe+Cw269dwChR1648FIXoGPNV1nuMBokZBqHNCWJFbGW59q+v+J3DSl9azbiif\noatn0Gj5QoA5YdIEf/FJP0qilQKBgQDBkkWuG0LnTOESOTq6SKyA6BRvkL6BZo7I\nDAtbq6AuDe08APQvjeS7qS65IMa8G1g0LsTOQia103RdradDK9mXYJHInhcqvI/z\nYT8bK8et7sNMAmQnVXrJc2bzKtESBydWrIaCEhJVoA7n3cBubazQjl2lzhrQQcaO\nNlqONXgLzwKBgBULGO3UT3j+sLKRmKy7Pn9qfiyOe83RbewVuC+KWb0Bc+kijl8P\nQc/GT45hB2UGj+cGa/4lJaDyET2YVoAUZ6vtjTTgZOiACaBlP7I51i6nc72k/VSF\nGpWLAYOAySA2WWh8RPug0PluK96vyxz6Ha1PlHc2O0zdOWRE+yre0FzpAoGAIE0A\nQ6VY7K/vftL98cfewUP45QUKRR2QBeG8o3doK0c3pyOhm63Z1VbN7SaDAzCs70g+\nnwx62SyH5nnrx1Gx5Yi/yvJWe1Mr0jkQBsCveV62SCC+BbJF4wfWzgz/BjsbXWM8\nrsfFN3SWCCsAyWyTwdl/1wMlhkrzjxeS1NgnSBECgYEAvgoUWmv7oMy+/lwXgx1m\nuCocZ8Vy5SFnamShTxTwFA1QayMGh3eFnVLilOmyHTtNo8hbcYOuSOrtoSePC/O5\n44pDwOGFoEH0XPs2JK2v4WAuDI3A0I4WAJC3QV/166mn+frrq1fIAE/A9z1HQFti\n17WGdu5QFyNnHvDscflc1OU=\n-----END PRIVATE KEY-----\n",
  "client_email": "523214905658-compute@developer.gserviceaccount.com",
  "client_id": "118398233219130847683",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/523214905658-compute%40developer.gserviceaccount.com"
        }';
    }

    /*
     * NOTE: if the server is a shared hosting by third party company then private key should not be stored as a file,
     * may be better to encrypt the private key value then store the 'encrypted private key' value as string in database,
     * so every time before use the private key we can get a user-input (from UI) to get password to decrypt it.
     */
    public function uploadFile($bucketName, $fileContent, $cloudPath)
    {
        // connect to Google Cloud Storage using private key as authentication
        try {
            $storage = new StorageClient([
                'keyFile' => json_decode($this->privateKeyFileContent, true)
            ]);

        } catch (Exception $e) {
            // maybe invalid private key ?
            print $e;
            return false;
        }

        // set which bucket to work in
        $bucket = $storage->bucket($bucketName);

        // upload/replace file
        $storageObject = $bucket->upload(
            $fileContent,
            ['name' => $cloudPath]
        // if $cloudPath is existed then will be overwrite without confirmation
        // NOTE:
        // a. do not put prefix '/', '/' is a separate folder name  !!
        // b. private key MUST have 'storage.objects.delete' permission if want to replace file !
        );

        // is it succeed ?

        return $storageObject != null;
    }

    /**
     * @param $bucketName
     * @param $cloudPath
     * @return array|bool
     * this (listFiles) method not used in this example but you may use according to your need
     */
    public function getFileInfo($bucketName, $cloudPath)
    {
        // connect to Google Cloud Storage using private key as authentication
        try {
            $storage = new StorageClient([
                'keyFile' => json_decode($this->privateKeyFileContent, true)
            ]);
        } catch (Exception $e) {
            // maybe invalid private key ?
            print $e;
            return false;
        }

        // set which bucket to work in
        $bucket = $storage->bucket($bucketName);
        $object = $bucket->object($cloudPath);
        return $object->info();
    }

    public function listFiles($bucket, $directory = null)
    {

        if ($directory == null) {
            // list all files
            $objects = $bucket->objects();
        } else {
            // list all files within a directory (sub-directory)
            $options = array('prefix' => $directory);
            $objects = $bucket->objects($options);
        }

        foreach ($objects as $object) {
            print $object->name() . PHP_EOL;
            // NOTE: if $object->name() ends with '/' then it is a 'folder'
        }
    }

}

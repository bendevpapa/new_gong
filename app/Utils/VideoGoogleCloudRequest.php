<?php


namespace App\Utils;


use Exception;
// use Google\Cloud\Storage\StorageClient;
use Google\Cloud\VideoIntelligence\V1\VideoIntelligenceServiceClient;

class VideoGoogleCloudRequest{

    /**
     * @var string
     * private key generated from google console
     */
    private $privateKeyFileContent;

    /**
     * GoogleCloudRequest constructor.
     */
    public function __construct(){
        $this->privateKeyFileContent = '{
            "type": "service_account",
            "project_id": "proxx-1574144707665",
            "private_key_id": "6888ffc946949972d5e5020e153f29a286ba6a15",
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDYILFZmHkF0HET\nU7JjLnJODqqlb0CBUxFNNTh7CfG1Ibtmv/0EtJLbk/XLWBh0Q4/TzzWwRg767ogd\n6PMPgBDIr+9jjpiga7Md6IPI/Om6wmZEJdbGKMxVXymz1Pj67uTwgI/xaE8uUvdR\npMJz97vaehZ6kdOpv+yxD5s8mo3mPOw6F9UhNoxxowEPJwKH3OQGpsqYxvUBQxqb\nn4TCiEH5eSNsrjAxKv1h6piC89FyNcqMYboAH9RXoGg2PFUq16Fs5ri12V1qasBS\npadlfDIRTylhc3QgOVmd4u+c/0aCgLn1qkURjbb5M7f1zU4xaPGT/HOj1V3upjDQ\nr5EW1lRbAgMBAAECggEAJ2Fxn08yghY0fF6MSxkr98AhZOqMGORvax+O7pBly+jd\nEkBaVJCoj7z6lPY04n5rBx+i+z8QvLVY9QY8v67IoCC6oGvbsKNfLExxMt6uofqJ\nEtLIOyw06ztfSGa+hU6hRZdPYx/sCZdRfD1mtyXFQ25qiSPoKu6PLMYYtaa9w10x\nv2Y5w58zhX10nkgYmQXPSZIc8/ObVGUxfNYGREx7eJUimWL8UlYfE4JspbWkUoQO\nVQle9kQSD488veCCr3Zm06qoBtF3wCuH5CmR93e6/7/zg3S2K/lwhlk1dnY1Hbc6\nRZKjp5+Cht00Raibsgn6GFatovWPl5qPdjBwAkxDAQKBgQDsEQMxyvBNwv1eZG3g\nvcPhTMUZaJzbR/EOeekGsyOoD9q3itc2W/Lae02210IVeSPC+JIou8e+Dbr/hZKS\nJK6AMYuEQdpfaIKL57yyuZeuA7C/74D6GrGC9rylKNUESG7XNGe/95p4w0RAXdEM\nJ6rYxb7K37rNW/Tdntml3k3VWwKBgQDqYKtlasUL70sXIjPQCJ+GxmoGjV/6fSpY\n1e4JIZae4n/LJsgzZbYpGrIv+DMh5VFsr95cDijEST8hbaHfELpHfHldIcg00jDr\nrMSSraK2jOrzN6chb7s23BtUVTzCBPt+hA6EftgGQrfYAKs/WuyKAxnIlmpdso6H\nq036Ky6tAQKBgGthVbWpnWOR5dSxFDYdu+NZG3LyXh90+xbdp30PsZF6HMBOnJSy\ngKaXXgNkqT/BSd4fWcTDnhk6f6xJoyFtJB9V6iOwHNg3XuKq9+/IclGKhjTYii+6\nQrLS736lMzhuhlTIFsWAqLh0AjDRPA0uYg2W7bG00XEmehzxbAmCNJ9vAoGAPhDp\ncorqsXVl8jNZey69aAhlDWUk1IWO0EWeqoZdh6e9ZcPYYlv9+Daclz1n9QLwJBMS\nlFgfjZNEB4srwZGBcurQjMwkY3CCCfcPezRGEYeRtGS5eo5HCokRJ60G0O5W1zML\nUSTx+f5zBt1ue/GNECMwEcFbW4KsYdsIZEZJLwECgYBsbRX6h3WixFqGM7yRUC0i\n6txl2y2Jar2outRcqDwdzwU0mwMVDeFuYUjqK7Wene4HPymvMHKy1PmD0hKKEhwB\nMhOKgt6TGk644rpeLojdEifgHzwWbPnYaCUAQY3P8I7tmEBjIMrZ4tQMXZYKKyJF\nhWYXyQUZJkYmx4gknSVdEQ==\n-----END PRIVATE KEY-----\n",
            "client_email": "369137912545-compute@developer.gserviceaccount.com",
            "client_id": "110720683963454588440",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/369137912545-compute%40developer.gserviceaccount.com"
        }';
    }

    /*
     * NOTE: if the server is a shared hosting by third party company then private key should not be stored as a file,
     * may be better to encrypt the private key value then store the 'encrypted private key' value as string in database,
     * so every time before use the private key we can get a user-input (from UI) to get password to decrypt it.
     */
    public function uploadFile($bucketName, $fileContent, $cloudPath)
    {
        // connect to Google Cloud Storage using private key as authentication
        try {
            $storage = new StorageClient([
                'keyFile' => json_decode($this->privateKeyFileContent, true)
            ]);
        } catch (Exception $e) {
            // maybe invalid private key ?
            print $e;
            return false;
        }

        // set which bucket to work in
        $bucket = $storage->bucket($bucketName);

        // upload/replace file
        $storageObject = $bucket->upload(
            $fileContent,
            ['name' => $cloudPath]
        // if $cloudPath is existed then will be overwrite without confirmation
        // NOTE:
        // a. do not put prefix '/', '/' is a separate folder name  !!
        // b. private key MUST have 'storage.objects.delete' permission if want to replace file !
        );

        // is it succeed ?
        return $storageObject != null;
    }

    /**
     * @param $bucketName
     * @param $cloudPath
     * @return array|bool
     * this (listFiles) method not used in this example but you may use according to your need
     */
    public function getFileInfo($bucketName, $cloudPath)
    {
        // connect to Google Cloud Storage using private key as authentication
        try {
            $storage = new StorageClient([
                'keyFile' => json_decode($this->privateKeyFileContent, true)
            ]);
        } catch (Exception $e) {
            // maybe invalid private key ?
            print $e;
            return false;
        }

        // set which bucket to work in
        $bucket = $storage->bucket($bucketName);
        $object = $bucket->object($cloudPath);
        return $object->info();
    }

    public function listFiles($bucket, $directory = null)
    {

        if ($directory == null) {
            // list all files
            $objects = $bucket->objects();
        } else {
            // list all files within a directory (sub-directory)
            $options = array('prefix' => $directory);
            $objects = $bucket->objects($options);
        }

        foreach ($objects as $object) {
            print $object->name() . PHP_EOL;
            // NOTE: if $object->name() ends with '/' then it is a 'folder'
        }
    }

}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Modals\Competition; 
use App\Modals\CompetitionUserJudge; 
use App\Modals\CompetitionUser; 
use App\Modals\UserContent; 
use App\Modals\UserTier; 
use App\Modals\Taxonomie;
use DB;

class CustomCompetitionEnd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:competitionEnd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $n = 23;
        // $a = $this->hasArrayTwoCandidates($n);
        // if($a[1] != 0){
        //     dd($a);
        // }else{
        //     $s = [];
        //     $re = [];
        //     for($i = $n; $i > 0; $i--){ //22
        //         $s = $this->hasArrayTwoCandidates($i);
        //         if($s[1] != 0){
        //             break;
        //         }
        //         $re[] = $n-$i;
        //         $n--;


        //     }
        //     dd($s);
        // }
        // exit;
        // dd(date('Y-m-d H:i:s'));
        $competition = Competition::where('is_active',1)
                                    ->where('comopetition_by_type','custom')
                                    ->get();
        $arr = [];
        $winner_whole_competition = 0;
       
        if(count($competition) > 0){
            $off_competition = [];
            $diff_in_minutes = 0;
            $arr = $competition->pluck('parent_id')->toArray();
            // dd(array_unique($arr));
            foreach ($competition as $key => $value) {
                $to = \Carbon\Carbon::now();
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->end_at);
                $diff_in_minutes = $to->diffInMinutes($from);
                if($diff_in_minutes < 50){
                    $off_competition[] = $value->id;
                   // Competition::where('id',$value->id)->update(['is_active'=>0]);
                    foreach ($value->competitionDetail as $key => $data) {
                        $data->userContent->update(['is_competition'=>0]);
                        $data->userContent->userTier->update(['is_competition'=>0]);
                        if($key == 0){
                            $competition_user_winner  =  DB::table(DB::raw('(select count("competition_user_id") as winner,competition_user_id
                                                 from competition_user_judge
                                                  where competition_id = '.$value->id.' and expression = 1 GROUP BY competition_user_id
                                                ) AS temp '))->select(DB::raw( '
                                             Max(winner),temp.competition_user_id'))->groupBy('competition_user_id')->first();
                            // dd($competition_user_winner);
                            if($competition_user_winner){
                                $c_id = $competition_user_winner->competition_user_id;
                                $competition_user = CompetitionUser::where('id',$c_id)->with('userContent.userTier')->first();
                                // $id = (int)$competition_user->userContent->userTier->taxonomie_id + (int)1;
                                // $taxnomie = Taxonomie::where('id',$id)->first();
                                // $user_level = (int)$taxnomie->level - (int)1;
                                // if($user_level < 10){
                                   $competition_user->winner   = 1;
                                   // $competition_user->is_added = 1;
                                   $competition_user->save();
                                   // $competition_user->userContent->userTier->update(['taxonomie_id'=>$taxnomie->id]);
                                   echo 'done';
                                // }  
                            }else{
                                $ad = $value->toArray();
                                $data->userContent->update(['is_competition'=>0]);
                                $data->userContent->userTier->update(['is_competition'=>0]);
                                $c_id = $ad['competition_detail'][1]['id'];
                                $competition_user = CompetitionUser::where('id',$c_id)->with('userContent.userTier')->first();
                                $competition_user->winner   = 1;
                                // $competition_user->is_added = 1;
                                $competition_user->save();
                                // echo '<<< heeellloooo>>>>';
                            } 
                        }   
                    }
                }else{
                    echo 'time left';

                }
            }
            if($diff_in_minutes < 50){
                $parent_ids[] = array_unique($arr);
                $p_id = 0;

                for($i = 0; $i<count($parent_ids); $i++){
                    $next_check = Competition::where('parent_id',$parent_ids[$i])
                                            ->where('is_active',1)
                                            ->get();
                    
                    $competition_ = Competition::where('id',$parent_ids[$i])
                                                ->with('activeChildCompetition.winnerNotFurtherUsedCompetitionDetail','getRoundCompetitior')
                                                ->first();
                    $last_competition_ = Competition::where('parent_id',$parent_ids[$i])
                                                ->orderBy('id','desc')
                                                ->first();
                    $p_id = $parent_ids[$i];
                    $no_round = $competition_->getRoundCompetitior->no_of_rounds; 
                    $round    = json_decode($competition_->getRoundCompetitior->round_details);
                    $round = (array)$round;
                    $number_of_competitor_each = 0;
                    $number_of_competition = 0;
                    if($competition_->competition_type == 'private'){
                        $code = rand(1021145,2546498);
                    }
                    $nu = (int)$last_competition_->runtime_round_check + (int)1;
                    if(count($next_check) == 1){
                            $competition_last = Competition::where('parent_id',$competition_->id)
                                                            ->orderBy('id','desc')
                                                            ->first();
                            // dd($competition_last);
                            if($competition_last){
                                // foreach ($competition_last->activeLastChildCompetition as $key => $response) {
                                    if($competition_last->winnerNotFurtherUsedCompetitionDetail){
                                        $user_t = UserTier::where('category_id',$competition_last->category_id)
                                                                        ->where('user_id',$competition_last->winnerNotFurtherUsedCompetitionDetail->user_id)
                                                                        ->first();
                                        $texnomie_id = $user_t->taxonomie_id + 1;
                                        $taxnomie = Taxonomie::where('id',$texnomie_id)->first();
                                        $user_level = (int)$taxnomie->level - (int)1;
                                        if($user_level < 10){
                                            $user_t->taxonomie_id = $texnomie_id;
                                            $user_t->save();
                                        }
                                    }  
                            }
                        continue;
                    }
                    foreach ($round['round'.$nu] as $key => $value) { 
                        $number_of_competitor_each = $value->no_of_competitor;
                        $number_of_competition_each = $value->total_competition;
                        $r = $competition_->runtime_round_check+1;
                        // if($last_competition_->runtime_round_check < count($round)){
                        if($last_competition_->runtime_round_check < count($round)){
                            
                            for($i = 1; $i<=$value->total_competition; $i++){ //6
                                $a = 0;
                                $competition_sub = new Competition();
                                $competition_sub->competition_name      = $competition_->competition_name;
                                $competition_sub->competition_end_type  = $competition_->competition_end_type;
                                $competition_sub->category_id           = $competition_->category_id;
                                $competition_sub->parent_id             = $competition_->parent_id;
                                $competition_sub->genre_id              = $competition_->genre_id;
                                $competition_sub->competition_media     = $competition_->competition_media;
                                $competition_sub->no_of_competitiors    = $number_of_competitor_each;
                                $competition_sub->no_of_rounds          = 1; 
                                $competition_sub->runtime_round_check   = $competition_->runtime_round_check + 1;
                                $competition_sub->competitior_set_id    = $competition_->competitior_set_id; 
                                $competition_sub->voting_interval       = $competition_->voting_interval;
                                $competition_sub->duration              = $competition_->duration;
                                $competition_sub->competition_type      = $competition_->competition_type;
                                $competition_sub->competition_set       = $competition_->competition_set;
                                $competition_sub->eligible_tier_id      = $competition_->eligible_tier_id;
                                $competition_sub->competition_user_level_id      = $competition_->competition_user_level_id;
                                $competition_sub->country_id            = $competition_->country_id;
                                $competition_sub->region_id             = $competition_->region_id;
                                $competition_sub->parent_id             = $p_id[0];
                                $competition_sub->comopetition_by_type  = 'custom';
                                $competition_sub->competition_by        = $competition_->competition_by;
                                $competition_sub->is_active             = 5; //after one hour competition auto start
                                $competition_sub->hold                  = date("Y-m-d H:i:s", strtotime('+1 hours'));
                                if(request()->access == 'private'){
                                    $competition_sub->joining_code = $code;
                                }
                                $is_saved = $competition_sub->save();
                                $competition_ab = Competition::where('parent_id',$competition_->id)
                                                                ->where('is_active',1)
                                                                ->with('winnerNotFurtherUsedCompetitionDetail','getRoundCompetitior')
                                                                ->limit($number_of_competitor_each)
                                                                ->get();

                                foreach ($competition_ab as $key => $response) { //4
                                    // if($response->winnerNotFurtherUsedCompetitionDetail){ //yes
                                        $a = $key + 1;
                                    // }
                                   
                                    if($a <= $number_of_competitor_each){  
                                        if($response->winnerNotFurtherUsedCompetitionDetail){
                                            CompetitionUser::where('id',$response->winnerNotFurtherUsedCompetitionDetail->id)->update(['is_added'=>1]);
                                            $user_c = UserContent::where('id',$response->winnerNotFurtherUsedCompetitionDetail->user_content_id)->update(['is_competition'=>1]);
                                            $user_t = UserTier::where('category_id',$competition_->category_id)
                                                                ->where('user_id',$response->winnerNotFurtherUsedCompetitionDetail->user_id)
                                                                ->update(['is_competition'=>1]);
                                            $competition_user = new CompetitionUser();
                                            $competition_user->user_id         = $response->winnerNotFurtherUsedCompetitionDetail->user_id;
                                            $competition_user->user_content_id = $response->winnerNotFurtherUsedCompetitionDetail->user_content_id;
                                            $competition_user->competition_id  = $competition_sub->id;
                                            $is_c_saved = $competition_user->save();
                                        }else{
                                            echo '<<<<<<<Not Winner loop check >>>>>>';
                                        }
                                    }
                                    Competition::where('id',$response->id)->update(['is_active' => 7]);
                                }
                            }
                        }
                    }
                    // if($last_competition_->runtime_round_check == count($round)){
                    //     $competition_last = Competition::where('parent_id',$competition_->id)
                    //                                     ->orderBy('id','desc')
                    //                                     ->first();
                    //     // dd($competition_last);
                    //     if($competition_last){
                    //         // foreach ($competition_last->activeLastChildCompetition as $key => $response) {
                    //             if($competition_last->winnerNotFurtherUsedCompetitionDetail){
                    //                 $user_t = UserTier::where('category_id',$competition_last->category_id)
                    //                                                 ->where('user_id',$competition_last->winnerNotFurtherUsedCompetitionDetail->user_id)
                    //                                                 ->first();
                    //                 $texnomie_id = $user_t->taxonomie_id + 1;
                    //                 $taxnomie = Taxonomie::where('id',$texnomie_id)->first();
                    //                 $user_level = (int)$taxnomie->level - (int)1;
                    //                 if($user_level < 10){
                    //                     $user_t->taxonomie_id = $texnomie_id;
                    //                     $user_t->save();
                    //                 }
                    //             }  
                    //     }
                        
                    // }
                }
                Competition::whereIn('id',$off_competition)->update(['is_active'=>0]);
            }
        }
    }

    function hasArrayTwoCandidates($val) { 
        switch ($val) {
            case $val%4==0:
                return [4,$val/4];
                break;
            case $val%5==0:
                return [5,$val/5];
                break;
            case $val%6==0:
                return [6,$val/6];
                break;
            case $val%3==0:
                return [3,$val/3];
                break;
            case $val%2==0:
                return [2,$val/2];
                break;
            
            default:
                return [0,0];
                break;
        }
    } 
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modals\Competition; 
use App\Modals\CompetitionUserJudge; 
use App\Modals\CompetitionUser; 
use App\Modals\UserContent; 
use App\Modals\UserTier; 
use App\Modals\Taxonomie;
use DB;

class HoldCompetitionStart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hold:competition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hold_competition = Competition::where('is_active',5)
                                        ->get();
        if($hold_competition){
            foreach ($hold_competition as $key => $value) {
                $to = \Carbon\Carbon::now();
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->hold);
                $diff_in_minutes = $to->diffInMinutes($from);
                // dd(date('Y-m-d H:i:s'));
                if($diff_in_minutes < 50){
                    Competition::where('id',$value->id)->update([
                                                                    'is_active' => 1,
                                                                    'start_at'  => date('Y-m-d H:i:s'),
                                                                    'end_at'    => date('Y-m-d H:i:s',strtotime('+'.$value->voting_interval.' hours'))
                                                                ]);
                }else{
                    echo "Time Left";
                }
            }
        }
    }
}

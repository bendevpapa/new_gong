<?php

namespace App\Modals\Traits;

trait ResponseMapper
{

    // Holds the value of error
    protected $error = '';

    // Hold the data to be sent in case of success
    protected $payload = '';
    protected $message = '';
    protected $responseCode = '';

    /**
     * @param $data
     * Sends response to the request.
     * @return array
     *
     */
    public function setPagination($data)
    {
        return [
            "page" => $data->currentPage(),
            "pageSize" => $data->perPage(),
            "totalPage" => (int) ($data->total() / $data->perPage()) + 1,
            "totalRecords" => $data->total()
        ];
    }

    public function sendJsonResponse()
    {
        $returnArr = array(
            'responseCode' => '',
            'message' => '',
            'httpCode' => '',
            'response' => '',
            'error' => null
        );

        if ($this->error) {
            $returnArr['responseCode'] = !empty($this->responseCode) ? $this->responseCode : 400;
            $returnArr['httpCode'] = 200;
            if ($this->message == trim($this->message) && strpos($this->message, ' ') !== false) {
                $returnArr['message'] = ucfirst($this->message);
            } else {
                $returnArr['message'] = trans('messages.error.' . $this->message . '.message');
            }
            $returnArr['error'] = $this->error ?? trans('messages.error.' . $this->error . '.message');
            if (is_array($this->payload)) {
                $returnArr['response'] = [];
            } else {
                $returnArr['response'] = (object) [];
            }
        } else {
            if (is_object($this->payload)) {
                // If payload is an eloquent object
                $returnArr['response'] = $this->payload->toArray();
            } else {
                if (is_array($this->payload)) {
                    $returnArr['response'] = $this->payload;
                } else {
                    if ($this->message != '') {
                        $returnArr['message'] = $this->message;
                    } else {
                        $returnArr['message'] = trans('messages.success.' . $this->payload . '.message');
                    }
                    $returnArr['response'] = (object) [];
                }
            }
            if ($this->message != '') {
                $returnArr['message'] = $this->message;
            }
            $returnArr['responseCode'] = !empty($this->responseCode) ? $this->responseCode : 200;
            $returnArr['httpCode'] = 200;
        }

        return response()->json([
            'responseCode' => $returnArr['responseCode'],
            'statusCode' => $returnArr['httpCode'],
            'message' => $returnArr['message'],
            'data' => $returnArr['response'],
            'error' => $returnArr['error']
        ], $returnArr['responseCode']);
    }
}

<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
// use App\User;
use Auth;
class CompetitionUser extends Model{
    protected $table = 'competition_users';
    protected $guarded = ['id'];
    public function user(){
    	return $this->belongsTo(User::class,'user_id','id');
    }
    public function userContent(){
    	return $this->belongsTo(UserContent::class,'user_content_id','id');
    }
    public function userLikes(){
    	return $this->hasMany(CompetitionUserLike::class,'competition_user_id','id');
    }
    public function userJudge(){
        if(Auth::check()){
           return $this->hasOne(CompetitionUserJudge::class,'competition_user_id','id')->where('user_judge_id',Auth::user()->id); 
       }else{
        return $this->hasOne(CompetitionUserJudge::class,'competition_user_id','id');
       }
        
    }

}

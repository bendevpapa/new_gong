<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function competition()
    {
        return $this->belongsTo(Competition::class, 'competition_id');
    }
}

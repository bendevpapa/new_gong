<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Taxonomie extends Model{
    //
	protected $fillable = [
        'id','name',
    ];

    public function mediaGenre(){
    	return $this->hasMany(Taxonomie::class,'parent_id','id')->where('type','video_level');
    }

    public function competitions()
    {
        return $this->hasMany(Competition::class, 'category_id');
    }

    public function competitions_sub()
    {
        return $this->hasMany(Competition::class, 'genre_id');
    }
}

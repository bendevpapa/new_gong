<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class CompetitionUserJudge extends Model{
    protected $table = 'competition_user_judge';
    public function competitionUser(){
    	return $this->belongsTo(CompetitionUser::class,'comopetition_user_id','id');
    }
}

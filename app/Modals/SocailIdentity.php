<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class SocailIdentity extends Model
{
    protected $fillable=['user_id','provider_name','provider_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}

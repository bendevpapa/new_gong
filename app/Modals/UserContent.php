<?php

namespace App\Modals;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserContent extends Model{

    // protected $fillable = [
    //     'user_id', 'file_type', 'file', 'file_gcs_self_link', 'category', 'name', 'genre_id', 'is_public', 'is_added_to_competition'
    // ];
    protected $guarded = ['id'];

    // ========================== ORM Start ======================= //

    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function userTier(){
        return $this->belongsTo(UserTier::class,'user_tier_id','id');
    }

    // ========================== ORM End ======================= //

    /**
     * @param array $fields
     * @return array
     */
    public function addContent(array $fields){
        // $data = [];
        // $file = $fields['file'];
        // $bucketName = config('constant.GCS_BUCKET');
        // // $bucketName = env('GCS_BUCKET', 'gong_bucket');
        // $fileContent = file_get_contents($file->getRealPath());

        // $cloudPath = 'uploads/'. $fields['file_type'] . '/' . time() . $file->getClientOriginalName();
        // $googleRequest = new GoogleCloudRequest();
        // $isSucceed = $googleRequest->uploadFile($bucketName, $fileContent, $cloudPath);

        // if ($isSucceed){
        //     $data['status'] = true;
        //     $data['message'] = 'content uploaded successfully';

        //     $fileInfo = $googleRequest->getFileInfo($bucketName, $cloudPath);
        //     $fields['file_gcs_self_link'] = $fileInfo['selfLink'];
        //     $fields['file'] =  $cloudPath;

        //     $userContent = $this::create($fields);
        //     $data['data'] = [
        //         'file_link' => config('constant.GCS_BASE_URL') .config('constant.GCS_BUCKET') . '/' . $cloudPath,
        //         // 'file_link' => env('GCS_BASE_URL') .env('GCS_BUCKET') . '/' . $cloudPath,
        //         'content_id' => $userContent->id
        //     ];

        //     // check if we have 3 submissions for same category, file type and tier

        // }else{
        //     $data['status'] = false;
        //     $data['message'] = 'There is problem uploading content';
        //     $data['data'] = [];
        // }

        // return $data;
    }
}

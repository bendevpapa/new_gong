<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class UserTier extends Model{


    protected $table = 'user_tiers';
    protected $guarded = ['id'];
    public function tier(){
    	return $this->belongsTo(Taxonomie::class,'taxonomie_id','id')->where('type','user_level');
    }
}

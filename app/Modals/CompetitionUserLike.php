<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class CompetitionUserLike extends Model{
    protected $table = 'competition_user_likes';
}

<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Region extends Model
{
    protected $fillable = [
        'country_id', 'region_name'
    ];

    // ========================== ORM Start ======================= //

    /**
     * @return BelongsTo
     */
    public function country(){
        return $this->belongsTo(Country::class);
    }

    /**
     * @return HasMany
     */
    public function userDetails(){
        return $this->hasMany(User::class);
    }

    // ========================== ORM End ======================= //
}

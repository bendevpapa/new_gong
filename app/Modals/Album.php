<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table='albums';

    public $timestamps = false;

    protected $fillable=['name','description','cover_image'];

    public function photos(){
        return $this->hasMany(Photo::class,'album_id');
    }
}

<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Country extends Model{

    /**
     * @var array
     */
    protected $fillable = [
        'iso2','iso3','capital','phonecode','capital','currency','flag', 'name'
    ];

    // ========================== ORM Start ======================= //

    /**
     * @return HasMany
     */
    public function userDetails(){
        return $this->hasMany(UserDetail::class);
    }

    /**
     * @return HasMany
     */
    public function regions(){
        return $this->hasMany(Region::class);
    }
    // ========================== ORM End ======================= //
}

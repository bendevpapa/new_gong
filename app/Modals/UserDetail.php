<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserDetail extends Model
{
    protected $fillable = [
        'user_id', 'name', 'username', 'date_of_birth', 'gender', 'profile_photo', 'region_id', 'country_id',
        'notify_by_app','notify_by_text'
    ];

    // ========================== ORM Start ======================= //

    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function country(){
        return $this->belongsTo(Country::class);
    }

    /**
     * @return BelongsTo
     */
    public function region(){
        return $this->belongsTo(State::class,'region_id','id');
    }

    // ========================== ORM End ======================= //
}

<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles;

    private $userDetail;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'phone', 'password', 'email_verified_at', 'taxonomie_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = bcrypt($pass);
    }

    public function identities(){
        return $this->hasMany(SocailIdentity::class);
    }



    /**
     * @return HasOne
     */
    public function userDetail()
    {
        return $this->hasOne(UserDetail::class);
    }

    /**
     * @return BelongsTo
     */
    public function tier()
    {
        return $this->belongsTo(Taxonomie::class, 'taxonomie_id', 'id')->where('type', 'user_level');
    }

    public function userTier()
    {
        return $this->hasMany(UserTier::class, 'user_id', 'id');
    }
    public function currentUserTier()
    {
        return $this->hasOne(UserTier::class, 'user_id', 'id')->orderBy('taxonomie_id', 'desc');
    }

    /**
     * @return HasMany
     */
    public function userContents()
    {
        return $this->hasMany(UserContent::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

}

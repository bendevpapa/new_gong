<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('login', 'Api\Auth\UserController@login');

Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
    Route::post('competition/create-competition', 'Api\CompetitionController@getCompetionCreate');
    Route::post('competition/judge', 'Api\CompetitionController@competitionUserJudge');
    Route::post('competition/single', 'Api\CompetitionController@getSingleCompetitionDetails');
    Route::get('competition/comments', 'Api\CompetitionController@getCompetitionComments');
    Route::post('competition/comment', 'Api\CompetitionController@getCompetitionCommentPost');
    Route::get('competition/getShareableLink', 'Api\CompetitionController@getShareableLink');

    Route::group(['prefix' => 'compete', 'as' => 'content.'], function () {
        Route::post('public/add', 'Api\UserCompeteController@addContentForPublicCompetition');

        Route::post('private/add', 'Api\UserCompeteController@addContentForPrivateCompetition');
    });

});
Route::post('register', 'Api\Auth\UserController@register');
Route::post('userDetail', 'Api\Auth\UserController@userDetail');
Route::post('forgot-password', 'Api\Auth\UserController@forgot_password');
Route::post('verify-number', 'Api\Auth\UserController@verifyPhoneNumber');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

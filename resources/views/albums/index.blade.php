@extends('layouts.app')

@section('content')
<main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Album </h1>

        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">
        @if(count($albums) > 0)
          <div class="row">
          @foreach($albums as $album)
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
               <img src="{!!checkImage(asset('storage/album_covers/'. $album->cover_image),'no_image.jpg')!!}" alt="">
                <div class="card-body">
                  <p class="card-text">{{$album->description}}</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                    </div>
                    <small class="text-muted">{{$album->name}}</small>
                  </div>
                </div>
              </div>
            </div>

            @endforeach

          </div>

          @else
          <h3>No album found</h3>

          @endif
        </div>
      </div>

    </main>

@endsection

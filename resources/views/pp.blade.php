
@extends('layouts.master')
@section('content')
<style>
    #verify-wrapper {
        font-family: Lato;
        font-size: 1.5rem;
        text-align: center;
        box-sizing: border-box;
        color: #333;
    }
    .verify-wrapper .dialog {
        border: solid 1px #ccc;
        margin: 10px auto;
        padding: 20px 30px;
        display: inline-block;
        box-shadow: 0 0 4px #ccc;
        background-color: #faf8f8;
        overflow: hidden;
        position: relative;
        max-width: 450px;
    }
    .verify-wrapper .dialog h3 {
        margin: 0 0 10px;
        padding: 0;
        line-height: 1.25;
    }
    .verify-wrapper .dialog span {
        font-size: 90%;
    }
    .verify-wrapper .dialog #form {
        max-width: 240px;
        margin: 25px auto 0;
    }
    .verify-wrapper .dialog #form input {
        margin: 0 5px;
        text-align: center;
        line-height: 80px;
        font-size: 50px;
        border: solid 1px #ccc;
        box-shadow: 0 0 5px #ccc inset;
        outline: none;
        width: 20%;
        -webkit-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
        border-radius: 3px;
    }
    .verify-wrapper .dialog #form input:focus {
        border-color: purple;
        box-shadow: 0 0 5px purple inset;
    }
    .verify-wrapper .dialog #form input::-moz-selection {
        background: transparent;
    }
    .verify-wrapper .dialog #form input::selection {
        background: transparent;
    }
    .verify-wrapper .dialog #form button {
        margin: 30px 0 50px;
        width: 100%;
        padding: 6px;
        border: none;
        text-transform: uppercase;
    }
    .verify-wrapper .dialog button.close {
        border: solid 2px;
        border-radius: 30px;
        line-height: 19px;
        font-size: 120%;
        width: 22px;
        position: absolute;
        right: 5px;
        top: 5px;
    }
    .verify-wrapper .dialog div {
        position: relative;
        z-index: 1;
    }
    .verify-wrapper .dialog img {
        position: absolute;
        bottom: -70px;
        right: -63px;
    }

</style>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#signUpModal">
                    Sign Up
                </button>
            </div>
        </div>
    </div>
</div>
<div class="verify-wrapper">
    <div class="dialog">
        <button class="close">×</button>
        <h3>Please enter the 4-digit verification code we sent via SMS:</h3>
        <span>(we want to make sure it's you before we contact our movers)</span>
        <div id="form">
            <input type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
            <input type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" /><input type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" /><input type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
            <button class="btn btn-primary btn-embossed">Verify</button>
        </div>

        <div>
            Didn't receive the code?<br />
            <a href="#">Send code again</a><br />
            <a href="#">Change phone number</a>
        </div>
    </div>
</div>

<div class="verify-wrapper">
    <div class="dialog">
        <button class="close">×</button>
        <h3>Change Phone Number</h3>
        <div id="form2" class="text-center">
            <input class="form-control mb-3" type="text" />
            <button class="btn btn-primary btn-embossed">Confirm</button>
        </div>

    </div>
</div>

<div class="footer"></div>





@endsection
@section('script')
@parent
<!--Bootstrap 4 JS files-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<!--Bootstrap 4 JS files-->


<script>
    $(function() {
        'use strict';

        var body = $('body');

        function goToNextInput(e) {
            var key = e.which,
                t = $(e.target),
                sib = t.next('input');

            if (key != 9 && (key < 48 || key > 57)) {
                e.preventDefault();
                return false;
            }

            if (key === 9) {
                return true;
            }

            if (!sib || !sib.length) {
                sib = body.find('input').eq(0);
            }
            sib.select().focus();
        }

        function onKeyDown(e) {
            var key = e.which;

            if (key === 9 || (key >= 48 && key <= 57)) {
                return true;
            }

            e.preventDefault();
            return false;
        }

        function onFocus(e) {
            $(e.target).select();
        }

        body.on('keyup', 'input', goToNextInput);
        body.on('keydown', 'input', onKeyDown);
        body.on('click', 'input', onFocus);

    })
</script>
@endsection
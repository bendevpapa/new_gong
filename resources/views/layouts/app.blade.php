
<head>

@include('layouts.head')
   
</head>
<body>
   @include('layouts.header')

   @section('content')

   @show

   @include('layouts.footer')


</body>
</html>

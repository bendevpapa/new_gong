@extends('layouts.master')
@section('content')
    <div class="banner-area" style="background: url({{asset('assets/images/gong-banner.png')}}) no-repeat center;background-size: cover">
        <div class="banner-content">
            <div class="container">
                <h1>Everybody has talent</h1>
                <p>You are just @if(Auth::check())
                                @if(Auth::user()->hasRole([1]))
                                  one
                                @else
                                 two 
                                @endif
                                @else
                                two
                                @endif
                 steps away from being able to share your talent with audiences throughout the world, build your fan base, connect with agents, and even earn tips and praise. To get going, simply</p>
                @if(Auth::guest())
                    <button class="btn btn-primary rounded-pill mb-3" data-backdrop="static" data-toggle="modal" data-target="#signUpModal">Join Now</button>
                @else
                 @if(Auth::user()->hasRole([1]))
                  <a href="{{route('user.dashboard')}}?is_compete=1" class="btn btn-primary rounded-pill mb-3" >Compete</a>
                 @endif
                @endif
                <p>& Upload your content</p>
            </div>
        </div>
    </div>
    <div class="tab-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                  <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link all_nav active" id="nav-Categories-tab" onclick="loadData('all',1,this)" data-toggle="tab" href="#nav-Categories" role="tab" aria-selected="true">
                        <div class="d-inline-block text-center">
                          <img class="passive-img" src="../assets/images/window-icon.png" alt="">
                          <img class="active-img" src="../assets/images/window-icon-active.png" alt="">
                          <p class="mb-0">All Categories</p>
                        </div>
                      </a>
                      <a class="nav-item nav-link all_nav" id="nav-Categories-tab" data-toggle="tab" onclick="loadData('vocal',1,this)" href="#nav-Categories" role="tab" aria-selected="false">
                        <div class="d-inline-block text-center">
                          <img class="passive-img" src="../assets/images/Vocals-icon.png" alt="">
                          <img class="active-img" src="../assets/images/Vocals-icon.png-active.png" alt="">

                          <p class="mb-0">Vocals</p>
                        </div>
                      </a>
                      <a class="nav-item nav-link all_nav"id="nav-Categories-tab" data-toggle="tab" onclick="loadData('dance',1,this)" href="#nav-Categories" role="tab" aria-selected="false">
                        <div class="d-inline-block text-center">
                          <img class="passive-img" src="../assets/images/dance-icon.png" alt="">
                          <img class="active-img" src="../assets/images/Dance-active.png" alt="">
                          <p class="mb-0">Dance</p>
                        </div>
                      </a>
                      <a class="nav-item nav-link all_nav" id="nav-Categories-tab" data-toggle="tab" onclick="loadData('comedy',1,this)" href="#nav-Categories" role="tab" aria-selected="false">
                        <div class="d-inline-block text-center">
                          <img class="passive-img" src="../assets/images/Comedy.png" alt="">
                          <img class="active-img" src="../assets/images/Comedy-active.png" alt="">
                          <p class="mb-0">Standup Comedy</p>
                        </div>
                      </a>
                      <a class="nav-item nav-link all_nav"id="nav-Categories-tab" data-toggle="tab" onclick="loadData('acting',1,this)" href="#nav-Categories" role="tab" aria-selected="false">
                        <div class="d-inline-block text-center">
                          <img class="passive-img" src="../assets/images/ActingIcon-2.png" alt="">
                          <img class="active-img" src="../assets/images/acting-icon.png" alt="">
                          <p class="mb-0">Acting</p>
                        </div>
                      </a>
                      <a class="nav-item nav-link all_nav" id="nav-Categories-tab" data-toggle="tab" onclick="loadData('modeling',1,this)" href="#nav-Categories" role="tab" aria-selected="false">
                        <div class="d-inline-block text-center">
                          <img class="passive-img" src="../assets/images/modling-icon.png" alt="">
                          <img class="active-img" src="../assets/images/Modeling-active.png" alt="">
                          <p class="mb-0">Modeling</p>
                        </div>
                      </a>
                      <a class="nav-item nav-link all_nav" id="nav-Categories-tab" data-toggle="tab" onclick="loadData('cheering',1,this)" href="#nav-Categories" role="tab" aria-selected="false">
                        <div class="d-inline-block text-center">
                          <img class="passive-img" src="../assets/images/Vocals-icon.png" alt="">
                          <img class="active-img" src="../assets/images/Vocals-icon.png-active.png" alt="">
                          <p class="mb-0">Funny Videos</p>
                        </div>
                      </a>
                    </div>
                  </nav>
                </div>
            </div>
        </div>
        <div class="tab-content" id="nav-tabContent" >
            {{-- div class="tab-pane fade show active" id="nav-Categories" role="tabpanel" aria-labelledby="nav-Categories-tab"> --}}
                @include('frontend.users.pages.home-tabs.all-categories')
            {{-- </div> --}}
           
    </div>
    <div class="app-area">
        <div class="container h-100">
          <div class="row align-items-center  h-100">
            <div class="col-md-6">
              <div class="app-area-content">
                <h3>DOWNLOAD</h3>
                <h1>Gong Talent</h1>
                <p>
                  <!-- We are all about connections, connecting talent with fans, agents with talent, and sponsors with the Gong community. A nobody, in small town America or a villager in Korea will now be able to perform before millions of people around the world, receive adulation, build an international fan-base, sell and/or promote their stuff, have one-click access to a list of agents and receive money from someone on the other side of the world while doing it. And the best part is there is no other app that does that today. -->
                  We are all about connections. Connecting talent with fans… agents with talent…and sponsors with the Gong community. A nobody, in small town America or a villager in Korea will now be able to perform before millions of people around the world, receive adulation, build an international fan-base, sell and promote their stuff, have one-click access to a list of agents and receive money from someone on the other side of the world while doing it. And the best part is there is no other app that does that today.
                </p>
                <div class="app-btn-group app-btn-group d-none ">
                  <a href="javascript:;">
                    <img src="../assets/images/app-store.png" alt="">
                  </a>
                  <a href="javascript:;">
                    <img src="../assets/images/ios@2x.png" alt="">
                  </a>
                </div>
                <div class="app-btn-group">

                  <a href="javascript:;" style="margin-top: 40px">
                    <div class="theme-timer ptc d-flex justify-content-around align-items-center">
                      <div id="flipdown" class="flipdown"></div>
                      <!-- <img src="../assets/images/timer.png" alt="timer.png"> -->
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div class="col-md-6 text-center">
              <img class="app-demo" src="../assets/images/mobile-screen.png" alt="">
            </div>
          </div>
        </div>
    </div>
      <div class="artist-area">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h2>Popular Artists</h2>
              <div class="artist-carousel owl-carousel owl-theme">
                @if(count($artists) <= 0)
                <div class="item">
                  <div class="text-center">
                    <img src="../assets/images/artist-1.png" alt="">
                  </div>
                </div>
                <div class="item">
                  <div class="text-center">
                    <img src="../assets/images/artist-2.png" alt="">
                  </div>
                </div>
                <div class="item">
                  <div class="text-center">
                    <img src="../assets/images/artist-3.png" alt="">
                  </div>
                </div>
                <div class="item">
                  <div class="text-center">
                    <img src="../assets/images/artist-4.png" alt="">
                  </div>
                </div>
                @else

                @foreach($artists as $key=>$response)
                @if($response->userDetail)
                  <div class="item">
                    <div class="text-center">
                      <img src="{{config('constant.BUCKET_URL') . $response->userDetail->profile_photo}}" alt="">
                    </div>
                  </div>
                  @endif
                @endforeach
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@include('frontend.users.js-css-blades.flipdown')

@endsection

@section('script')
@parent

    <script type="text/javascript">
         $('.audio-owl-slider').owlCarousel({
              loop:false,
              margin:18,
              nav:false,
              responsive:{
                0:{
                  items:1
                },
                600:{
                  items: 1
                },
                768:{
                  items:2
                }
              }
            });
        // var wavesurfer = Object.create(WaveSurfer);
        $(document).ready(function(){

        });

         document.addEventListener('DOMContentLoaded', () => {
          // Unix timestamp (in seconds) to count down to
          @php $date = date('Y-m-d'); @endphp
          var there = new Date("{{date('Y-m-d', strtotime($date.'+36 days'))}}");
          var here = changeTimezone(there, "{{date_default_timezone_get()}}");
          var twoDaysFromNow = (here/ 1000);
          // Set up FlipDown
          // console.log(here);
          var flipdown = new FlipDown(twoDaysFromNow)
            // Start the countdown
            .start()
            // Do something when the countdown ends
            .ifEnded(() => {
              console.log('The countdown has ended!');
            });
        });
          function changeTimezone(date, ianatz) {

          // suppose the date is 12:00 UTC
          var invdate = new Date(date.toLocaleString('en-US', {
            timeZone: ianatz
          }));

          // then invdate will be 07:00 in Toronto
          // and the diff is 5 hours
          var diff = date.getTime() - invdate.getTime();

          // so 12:00 in Toronto is 17:00 UTC
          return new Date(date.getTime() + diff);

        }
    </script>
@endsection

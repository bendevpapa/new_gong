
  
<div class="tab-pane fade show active" id="nav-Categories-tab" role="tabpanel" aria-labelledby="nav-Categories-tab">
        <div class="container no_competition" >
          <div class="alert alert-danger">
            <p>No Competition Created Yet </p>
          </div>
        </div>
        <div class="container">
          <div class="row grid-divider">
            <div class="col-lg-6 col-md-12 col-sm-12 premier_a">
              <div class="bg-green tab-widget-title">
                <h2>Premiers</h2>
                <h2><a class="view-all-cat" href="{{URL('get-competiton-all/')}}">View All</a></h2>
              </div>
              <div class="pug_loader_premier">
                  @include('frontend.users.pages.home-tabs.pug-loader')
              </div>
              <div class="post-audio-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="premier_audio audio_a audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all  audio_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="premier_img img_all_em">
                          <div class="owl-carousel owl-theme">
                          </div>
                           
                      </div>
                  </div>
              </div>
              <div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                     
                    <div class="premier_video vid_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class="no_premier_video_found no_found_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>

              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 winner_a">
              <div class="bg-purple tab-widget-title">
                <h2>Competition Winners</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_winner_audio ">
                  @include('frontend.users.pages.home-tabs.pug-loader')
              </div>
              <div class="post-audio-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="winner_vid audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                    
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="winner_img img_all_em">
                          <div class="owl-carousel owl-theme">
                          </div>
                          
                      </div>
                  </div>
              </div>

              <div class="post-video-grid border-0">
                <div class="video-slider">
                  <div class="video-posted ">
                    
                    <div class="winner_audio audio_all_em">
                      <div class="owl-carousel  owl-theme">
                      </div>
                      <div class="no_winner_comp_found no_found_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
          </div>
        </div>
       <!--  <div class="container mt-4">
          <div class="row grid-divider">
            <div class="col-lg-6 col-md-12 col-sm-12 popular_a">
              <div class="bg-purple-reverse tab-widget-title">
                <h2>Populars</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_popular">
                  @include('frontend.users.pages.home-tabs.pug-loader')
              </div>
              <div class="post-audio-grid ">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="popular_audio audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>

              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="popular_img img_all_em">
                          <div class="owl-carousel owl-theme">
                          </div>
                          <div class="no_found_winner no_found_all"></div>
                          
                      </div>
                  </div>
              </div>
              <div class="post-video-grid pb-0 mb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                    
                    <div class="popular vid_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 rising_star_a">
              <div class="bg-red-2 tab-widget-title">
                <h2>Rising Stars</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_rising_star">
                          @include('frontend.users.pages.home-tabs.pug-loader')
                      </div>
              <div class="post-audio-grid ">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="start_audio audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>

              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="rising_star_img img_all_em">
                          <div class="owl-carousel owl-theme">

                          </div>
                          
                      </div>
                  </div>
              </div>
              <div class="post-video-grid pb-0 mb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                   
                    <div class="rising_star vid_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class="no_found_star no_found_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>

              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
          </div>
        </div>

        <div class="container mt-4">
          <div class="row grid-divider">
            <div class="col-lg-6 col-md-12 col-sm-12 talent_a">
              <div class="bg-purple-reverse tab-widget-title">
                <h2>Got Talent</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_talent">
                        @include('frontend.users.pages.home-tabs.pug-loader')
                    </div>
              <div class="post-audio-grid ">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="got_talent_audio audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>



              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="talent_img img_all_em">
                          <div class="owl-carousel owl-theme">

                          </div>
                          
                      </div>
                  </div>
              </div>

              <div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                    
                    <div class="talent vid_all_em">
                      <div class="owl-carousel owl-theme">

                      </div>
                      <div class="no_found_talent no_found_all"></div>
                     
                    </div>
                  </div>
                </div>
              </div>

              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>



            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 professional_a">
              <div class="bg-red-2 tab-widget-title">
                <h2>Professional</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_professional">
                        @include('frontend.users.pages.home-tabs.pug-loader')
                    </div>
              <div class="post-audio-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="professional_audio audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                     
                    </div>
                  </div>
                </div>
              </div>



              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="professional_img img_all_em">
                          <div class="owl-carousel owl-theme">

                          </div>
                          
                      </div>
                  </div>
              </div>
                <div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                    
                    <div class="professional vid_all_em">
                      <div class="owl-carousel owl-theme">

                      </div>
                      <div class="no_found_professional no_found_all"></div>
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
          </div>
        </div>

        <div class="container mt-4">
          <div class="row grid-divider">
            <div class="col-lg-6 col-md-12 col-sm-12 serious_a">
              <div class="bg-purple-reverse tab-widget-title">
                <h2>Serious Talent</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_serious">
                        @include('frontend.users.pages.home-tabs.pug-loader')
                    </div>
              <div class="post-audio-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="serious_audio audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="serious_talent_img img_all_em">
                          <div class="owl-carousel owl-theme">

                          </div>
                          
                      </div>
                  </div>
              </div>
              <div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                    
                    <div class="serious vid_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class="no_found_serious no_found_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>

              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 potential_a">
              <div class="bg-red-2 tab-widget-title">
                <h2>Potential Stars</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_potential_star">
                        @include('frontend.users.pages.home-tabs.pug-loader')
                    </div>
              <div class="post-audio-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="potential_audio audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                    
                    </div>
                  </div>
                </div>
              </div>



              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="potential_img img_all_em">
                          <div class="owl-carousel owl-theme">

                          </div>
                         
                      </div>
                  </div>
              </div>
              <div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                    
                    <div class="potential_star vid_all_em">
                      <div class="owl-carousel owl-theme">

                      </div>
                      <div class="no_found_potential no_found_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>

              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
          </div>
        </div>

        <div class="container mt-4">
          <div class="row grid-divider">
            <div class="col-lg-6 col-md-12 col-sm-12 star_r_a">
              <div class="bg-purple-reverse tab-widget-title">
                <h2>Star</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_star_">
                        @include('frontend.users.pages.home-tabs.pug-loader')
                    </div>
              <div class="post-audio-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="star_audio audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                   
                    </div>
                  </div>
                </div>
              </div>




              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="star__img img_all_em">
                          <div class="owl-carousel owl-theme">

                          </div>
                         
                      </div>
                  </div>
              </div>
              <div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                    
                    <div class="star_ vid_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class="no_found_star_ no_found_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>

              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 super_a">
              <div class="bg-red-2 tab-widget-title">
                <h2>Super Stars</h2>
                <h2>View All</h2>
              </div>
              <div class="pug_loader_super_star">
                        @include('frontend.users.pages.home-tabs.pug-loader')
                    </div>
              <div class="post-audio-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="audio-posted mb-0 pb-0">
                    <div class="super_audio audio_all_em">
                      <div class="owl-carousel owl-theme">
                      </div>
                      <div class=" no_found_all audio_all"></div>
                     
                    </div>
                  </div>
                </div>
              </div>



              <div class="post-img-grid border-0 pb-0 mb-0">
                  <div class="img-posted pb-0 border-0">
                      
                      <div class="super_star_img img_all_em">
                          <div class="owl-carousel owl-theme">

                          </div>
                          
                      </div>
                  </div>
              </div>
              <div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                    
                    <div class="super_star vid_all_em">
                      <div class="owl-carousel owl-theme">

                      </div>
                      <div class="no_found__super_star no_found_all"></div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="btn-grid">
                           <div class="left-btn-area invisible invisible">
                               <div>
                                   <i class="fa fa-gavel" aria-hidden="true"></i> <span>20</span>
                               </div>
                               <div>
                                   <i class="fa fa-comments" aria-hidden="true"></i> <span>5</span>
                               </div>
                               <div>
                                   <i class="fa fa-share-square"></i> <span>15</span>
                               </div>
                           </div>
                           <div class="right-btn-area">
                               <div>
                                   <button class="btn btn-danger rounded-pill mini-btn mr-2 invisible "> HIPHOP </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill mr-2 invisible invisible">
                                       <i class="fa fa-info-circle color-dark invisible" aria-hidden="true"></i>
                                       View Stats
                                   </button>
                               </div>
                               <div>
                                   <button class="btn btn-primary rounded-pill btn_judge_link " data-url="">
                                       <i class="fa fa-gavel color-dark" aria-hidden="true"></i>
                                       Judge
                                   </button>
                               </div>
                           </div>
                       </div>
            </div>
          </div>
        </div> -->
      </div>

   @section('script')
   @parent
   <script type="text/javascript">
    var wavesurfer = [];
     $('.btn_play').hide();
     $('.btn-grid').hide();
     // $(document).ready(function(){
    // loadData('all',1);
    function loadData(nav,d,this_=""){

      var html = ``;
      $('.no_found_all').html('');
      $('.audio_all_em').find('.owl-carousel').html('');
      $('.img_all_em').find('.owl-carousel').html('');
      $('.vid_all_em').find('.owl-carousel').html('');
      $('.audio_all_em').parent().parent().parent().parent().show();
      $('.img_all_em').parent().parent().parent().show();
      $('.vid_all_em').parent().parent().parent().parent().show();
      $('.btn-grid').hide();
      $('.no_competition').hide();
      var popular_html = ``;
      var start_html = ``;
      var wiing_html = ``;

      var got_talent_html = ``;
      var professional_html = ``;

      var serious_html = ``;
      var potential_html = ``;

      var star_html = ``;
      var super_html = ``;
      if(d == 1){
        $('.all_nav').removeClass('active')
        $(this_).addClass('active');
        $('.pug_loader_premier').show();
        $('.pug_loader_popular').show();
        $('.pug_loader_rising_star').show();
        $('.pug_loader_winner_audio').show();

        $('.pug_loader_talent').show();
        $('.pug_loader_professional').show();
        $('.pug_loader_serious').show();
        $('.pug_loader_potential_star').show();

        $('.pug_loader_star_').show();
        $('.pug_loader_super_star').show();
      }
      axios({
              method: 'get',
              url: "{{route('user.get-competition-detail')}}?q="+nav,
              responseType: 'json'
            }).then(function (response) {
                // console.log(response.data.status);
                  if(response.data.status == 1){
                    var ite = 0;

                    $(".view-all-cat").attr('href', "{{url('get-competiton-all')}}" +"/"+ nav)
                    /**** actual parsing data ********/
                    if(response.data.competition){
                      $.each(response.data.competition.active_competition_detail,function(key,item){
                        if(response.data.competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          html += videoHtml(key,item);
                          $('.premier_a').find('.btn-grid').show().data('url',"{{route('voting')}}?q="+item.competition_id);
                          $('.premier_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);
                        }
                        if(response.data.competition.competition_media == 'audio'){
                          // html += audioHtml(key,item);
                          // $('.premier_video').html(audioHtml(key,item));
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.premier_audio').find('.owl-carousel').addClass('audio-owl-slider');
                          $('.premier_audio').find('.owl-carousel').append(audioHtml(key,item));
                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                          $('.premier_a').find('.btn-grid').show();
                          $('.premier_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);
                          // $('.premier_video').parent().parent().parent().hide();
                          // $('.premier_img').parent().parent().hide();
                        }
                        if(response.data.competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          html += imageHtml(key,item);
                          $('.premier_a').find('.btn-grid').show();
                          $('.premier_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }

                          ite++;
                      });

                    }else{
                      $('.premier_a').hide();
                      // html += 'No Competition Created Yet.';
                    }
                    // console.log(response.data.popular_competition);
                    if(response.data.popular_competition){

                      $.each(response.data.popular_competition.active_competition_detail,function(key,item){
                        // popular_html += videoHtml(item);

                        if(response.data.popular_competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          popular_html += videoHtml(key,item);
                          $('.popular_a').find('.btn-grid').show();
                          $('.popular_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                        }

                        if(response.data.popular_competition.competition_media == 'audio'){
                          // popular_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.popular_audio').find('.owl-carousel').append(audioHtml(key,item));
                          $('.popular_a').find('.btn-grid').show();
                          $('.popular_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);
                          // $('.popular_img').parent().parent().hide();
                          // $('.popular').parent().parent().parent().hide();

                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.popular_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          popular_html += imageHtml(key,item);
                          $('.popular_a').find('.btn-grid').show();
                          $('.popular_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                        }
                          ite++;
                      });
                    }else{
                      $('.popular_a').hide();
                      // popular_html += 'No Competition Created Yet.';
                    }

                    if(response.data.star_competition){
                      // console.log(response.data.star_competition);
                      $.each(response.data.star_competition.active_competition_detail,function(key,item){
                        // start_html += videoHtml(item);
                        // console.log(item);
                        if(response.data.star_competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          start_html += videoHtml(key,item);
                          $('.rising_star_a').find('.btn-grid').show();
                          $('.rising_star_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                        if(response.data.star_competition.competition_media == 'audio'){
                          // start_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.start_audio').find('.owl-carousel').append(audioHtml(key,item));
                          $('.rising_star_a').find('.btn-grid').show();
                          $('.rising_star_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);
                          // $('.rising_star').parent().parent().parent().hide();
                          // $('.rising_star_img').parent().parent().hide();

                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.star_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          start_html += imageHtml(key,item);
                          $('.rising_star_a').find('.btn-grid').show();
                          $('.rising_star_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                        }
                          ite++;
                      });
                    }else{
                      $('.rising_star_a').hide();
                      // start_html += 'No Competition Created Yet.';
                    }

                    if(response.data.winner_competition){
                      $.each(response.data.winner_competition.active_competition_detail,function(key,item){
                        // wiing_html += audioHtml(key,item);
                        if(response.data.winner_competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          wiing_html += videoHtml(key,item);
                          $('.winner_a').find('.btn-grid').show();
                          $('.winner_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                        if(response.data.winner_competition.competition_media == 'audio'){
                          // wiing_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.winner_vid').find('.owl-carousel').append(audioHtml(key,item));
                          $('.winner_a').find('.btn-grid').show();
                          $('.winner_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                          // $('.winner_audio').parent().parent().parent().hide();
                          // $('.winner_img').parent().parent().hide();
                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.winner_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          wiing_html += imageHtml(key,item);
                          $('.winner_a').find('.btn-grid').show();
                          $('.winner_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }

                      });
                    }else{
                      $('.winner_a').hide();
                      // wiing_html += 'No Competition Created Yet.';
                    }

                    if(response.data.talent_competition){
                      $.each(response.data.talent_competition.active_competition_detail,function(key,item){
                        // got_talent_html += videoHtml(key,item);
                        if(response.data.talent_competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          got_talent_html += videoHtml(key,item);
                          $('.talent_a').find('.btn-grid').show();
                          $('.talent_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                        if(response.data.talent_competition.competition_media == 'audio'){
                          // got_talent_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.got_talent_audio').find('.owl-carousel').append(audioHtml(key,item));
                          $('.talent_a').find('.btn-grid').show();
                          $('.talent_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                          // $('.talent').parent().parent().parent().hide();
                          // $('.talent_img').parent().parent().hide();
                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.talent_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          got_talent_html += imageHtml(key,item);
                          $('.talent_a').find('.btn-grid').show();
                          $('.talent_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                        }
                      });
                    }else{
                      $('.talent_a').hide();
                      // got_talent_html += 'No Competition Created Yet.';
                    }

                    if(response.data.professional_competition){
                      $.each(response.data.professional_competition.active_competition_detail,function(key,item){
                        // professional_html += videoHtml(key,item);
                        if(response.data.professional_competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          professional_html += videoHtml(key,item);
                          $('.professional_a').find('.btn-grid').show();
                          $('.professional_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                        if(response.data.professional_competition.competition_media == 'audio'){
                          // professional_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.professional_audio').find('.owl-carousel').append(audioHtml(key,item));
                          $('.professional_a').find('.btn-grid').show();
                          $('.professional_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                          // $('.professional').parent().parent().parent().hide();
                          // $('.professional_img').parent().parent().hide();
                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.professional_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          professional_html += imageHtml(key,item);
                          $('.professional_a').find('.btn-grid').show();
                          $('.professional_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                      });
                    }else{
                      $('.professional_a').hide();
                      // professional_html += 'No Competition Created Yet.';
                    }

                    if(response.data.serious_competition){

                      $.each(response.data.serious_competition.active_competition_detail,function(key,item){
                        // serious_html += videoHtml(key,item);

                        if(response.data.serious_competition.competition_media == 'video'){
                           key = Math.floor((Math.random() * 2452155550) + 1);
                          serious_html += videoHtml(key,item);
                          $('.serious_a').find('.btn-grid').show();
                          $('.serious_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                        // console.log(serious_html);
                        if(response.data.serious_competition.competition_media == 'audio'){
                          // serious_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.serious_audio').find('.owl-carousel').append(audioHtml(key,item));
                          $('.serious_a').find('.btn-grid').show();
                          $('.serious_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                          //  $('.serious').parent().parent().parent().hide();
                          // $('.serious_talent_img').parent().parent().hide();
                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.serious_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          serious_html += imageHtml(key,item);
                          $('.serious_a').find('.btn-grid').show();
                          $('.serious_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                      });

                    }else{
                      $('.serious_a').hide();
                      // serious_html += 'No Competition Created Yet.';
                    }

                    if(response.data.potential_competition){
                      $.each(response.data.potential_competition.active_competition_detail,function(key,item){
                        // potential_html += videoHtml(key,item);
                        if(response.data.potential_competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          potential_html += videoHtml(key,item);
                          $('.potential_a').find('.btn-grid').show();
                          $('.potential_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                        if(response.data.potential_competition.competition_media == 'audio'){
                          // potential_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.potential_audio').find('.owl-carousel').append(audioHtml(key,item));
                          $('.potential_a').find('.btn-grid').show();
                          $('.potential_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                          // $('.potential_star').parent().parent().parent().hide();
                          // $('.potential_img').parent().parent().hide();
                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.potential_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          potential_html += imageHtml(key,item);
                          $('.potential_a').find('.btn-grid').show();
                          $('.potential_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                      });
                    }else{
                      $('.potential_a').hide();
                      // potential_html += 'No Competition Created Yet.';
                    }

                    if(response.data.star_a_competition){
                      $.each(response.data.star_a_competition.active_competition_detail,function(key,item){
                        // star_html += videoHtml(key,item);
                        if(response.data.star_a_competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          star_html += videoHtml(key,item);
                          $('.star_r_a').find('.btn-grid').show();
                          $('.star_r_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                        if(response.data.star_a_competition.competition_media == 'audio'){
                          // star_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.star_audio').find('.owl-carousel').append(audioHtml(key,item));
                          $('.star_r_a').find('.btn-grid').show();
                          $('.star_r_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                          // $('.star_').parent().parent().parent().hide();
                          // $('.star__img').parent().parent().hide();
                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.star_a_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          star_html += imageHtml(key,item);
                          $('.star_r_a').find('.btn-grid').show();
                          $('.star_r_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                      });
                    }else{
                      $('.star_r_a').hide();
                      // star_html += 'No Competition Created Yet.';
                    }

                    if(response.data.super_star_competition){
                      $.each(response.data.super_star_competition.active_competition_detail,function(key,item){
                        // super_html += videoHtml(key,item);
                        if(response.data.super_star_competition.competition_media == 'video'){
                          key = Math.floor((Math.random() * 2452155550) + 1);
                          super_html += videoHtml(key,item);
                          $('.super_a').find('.btn-grid').show();
                          $('.super_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                        if(response.data.super_star_competition.competition_media == 'audio'){
                          // super_html += audioHtml(key,item);
                          key = Math.floor((Math.random() * 18885550) + 1);
                          $('.super_audio').find('.owl-carousel').append(audioHtml(key,item));
                          $('.super_a').find('.btn-grid').show();
                          $('.super_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);

                          $('.super_star').parent().parent().parent().hide();
                          // $('.super_star_img').parent().parent().hide();
                           wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.user_content.file, '#waveform_'+key);
                        }
                        if(response.data.super_star_competition.competition_media == 'image'){
                          key = Math.floor((Math.random() * 98885456550) + 1);
                          super_html += imageHtml(key,item);
                          $('.super_a').find('.btn-grid').show();
                          $('.super_a').find('.btn_judge_link').attr('data-url',"{{route('voting')}}?q="+item.competition_id);


                        }
                      });
                    }else{
                      $('.super_a').hide();
                      // super_html += 'No Competition Created Yet.';
                    }

                  /**********  end parsing ********/




                    html += ``;
                    // console.log(123);
                    $('.pug_loader_premier').hide();
                    $('.pug_loader_popular').hide();
                    $('.pug_loader_rising_star').hide();
                    $('.pug_loader_winner_audio').hide();

                    $('.pug_loader_talent').hide();
                    $('.pug_loader_professional').hide();
                    $('.pug_loader_serious').hide();
                    $('.pug_loader_potential_star').hide();

                    $('.pug_loader_star_').hide();
                    $('.pug_loader_super_star').hide();

                    $('.no_found_all').html('');
                    /*** conditional check ***/
                    // console.log(response.data.winner_competition)
                    if(response.data.competition){
                      if(response.data.competition.competition_media == 'video'){
                        $('.premier_video').find('.owl-carousel').html(html).show();
                        // $('.premier_video').html(html);
                        // $('.premier_img').parent().parent().hide();
                        // $('.premier_audio').parent().parent().parent().hide();
                      }
                      if(response.data.competition.competition_media == 'image'){
                        $('.premier_img').find('.owl-carousel').html(html).show();
                        // $('.premier_video').parent().parent().parent().hide();
                        // $('.premier_audio').parent().parent().parent().hide();
                      }
                    }else{
                       
                      $('.no_premier_video_found').html(html);
                    }

                    if(response.data.winner_competition){
                      // $('.winner_audio').find('.owl-carousel').html(wiing_html);
                      if(response.data.winner_competition.competition_media == 'video'){
                        $('.winner_audio').find('.owl-carousel').html(wiing_html).show();
                        // $('.winner_vid').parent().parent().parent().hide();
                        // $('.winner_img').parent().parent().hide();
                      }
                      if(response.data.winner_competition.competition_media == 'image'){
                        $('.winner_img').find('.owl-carousel').html(wiing_html).show();
                        // $('.winner_vid').parent().parent().parent().hide();
                        // $('.winner_audio').parent().parent().parent().hide();
                      }

                    }else{
                      $('.no_winner_comp_found').html(wiing_html).show();
                    }

                    if(response.data.popular_competition){
                       // $('.popular').find('.owl-carousel').html(popular_html);
                       if(response.data.popular_competition.competition_media == 'video'){
                        $('.popular').find('.owl-carousel').html(popular_html).show();
                        // $('.popular_img').parent().parent().hide();
                        // $('.popular_audio').parent().parent().parent().hide();
                       }
                       if(response.data.popular_competition.competition_media == 'image'){
                        $('.popular_img').find('.owl-carousel').html(popular_html).show();
                        // $('.popular').parent().parent().parent().hide();
                        // $('.popular_audio').parent().parent().parent().hide();
                       }

                    }else{
                      $('.no_found_popular').html(popular_html);
                    }

                    if(response.data.star_competition){
                       // $('.rising_star').find('.owl-carousel').html(start_html);
                       if(response.data.star_competition.competition_media == 'video'){
                        $('.rising_star').find('.owl-carousel').html(start_html);
                        // $('.rising_star_img').parent().parent().hide();
                        // $('.star_audio').parent().parent().parent().hide();
                       }
                       if(response.data.star_competition.competition_media == 'image'){
                        $('.rising_star_img').find('.owl-carousel').html(start_html);
                        // $('.rising_star').parent().parent().parent().hide();
                        // $('.star_audio').parent().parent().parent().hide();
                       }

                    }else{
                      $('.no_found_star').html(start_html);
                    }
                    /***********************************/
                    if(response.data.talent_competition){
                     // $('.talent').find('.owl-carousel').html(got_talent_html);
                     if(response.data.talent_competition.competition_media == 'video'){
                        $('.talent').find('.owl-carousel').html(got_talent_html);
                        // $('.talent_img').parent().parent().hide();
                        // $('.got_talent_audio').parent().parent().parent().hide();
                       }
                       if(response.data.talent_competition.competition_media == 'image'){
                        $('.talent_img').find('.owl-carousel').html(got_talent_html);
                        // $('.talent').parent().parent().parent().hide();
                        // $('.got_talent_audio').parent().parent().parent().hide();
                       }

                    }else{
                      $('.no_found_talent').html(got_talent_html);
                    }

                    if(response.data.professional_competition){
                     // $('.professional').find('.owl-carousel').html(professional_html);
                     if(response.data.professional_competition.competition_media == 'video'){
                        $('.professional').find('.owl-carousel').html(professional_html);
                        // $('.professional_img').parent().parent().hide();
                        // $('.professional_audio').parent().parent().parent().hide();

                       }
                       if(response.data.professional_competition.competition_media == 'image'){
                        $('.professional_img').find('.owl-carousel').html(professional_html);
                        // $('.professional').parent().parent().parent().hide();
                        // $('.professional_audio').parent().parent().parent().hide();

                       }

                    }else{
                      $('.no_found_professional').html(professional_html);
                    }

                    if(response.data.serious_competition){
                     // $('.serious').find('.owl-carousel').html(serious_html);
                     if(response.data.serious_competition.competition_media == 'video'){
                        $('.serious').find('.owl-carousel').html(serious_html);
                        // $('.serious_talent_img').parent().parent().hide();
                        // $('.serious_audio').parent().parent().parent().hide();
                       }
                       if(response.data.serious_competition.competition_media == 'image'){
                        $('.serious_talent_img').find('.owl-carousel').html(serious_html);
                        // $('.serious').parent().parent().parent().hide();
                        // $('.serious_audio').parent().parent().parent().hide();
                       }

                    }else{
                      $('.no_found_serious').html(serious_html);
                    }

                    if(response.data.potential_competition){
                     // $('.potential_star').find('.owl-carousel').html(potential_html);
                     if(response.data.potential_competition.competition_media == 'video'){
                        $('.potential_star').find('.owl-carousel').html(potential_html);
                        // $('.potential_img').parent().parent().hide();
                        // $('.potential_audio').parent().parent().parent().hide();
                       }
                       if(response.data.potential_competition.competition_media == 'image'){
                        $('.potential_img').find('.owl-carousel').html(potential_html);
                        // $('.potential_star').parent().parent().parent().hide();
                        // $('.potential_audio').parent().parent().parent().hide();
                       }

                    }else{
                      $('.no_found_potential').html(potential_html);
                    }

                    if(response.data.star_a_competition){
                     // $('.star_').find('.owl-carousel').html(star_html);
                     if(response.data.star_a_competition.competition_media == 'video'){
                        $('.star_').find('.owl-carousel').html(star_html);
                        // $('.star__img').parent().parent().hide();
                        // $('.star_audio').parent().parent().parent().hide();
                       }
                       if(response.data.star_a_competition.competition_media == 'image'){
                        $('.star__img').find('.owl-carousel').html(star_html);
                        // $('.star_').parent().parent().parent().hide();
                        // $('.star_audio').parent().parent().parent().hide();
                       }

                    }else{
                      $('.no_found_star_').html(star_html);
                    }

                    if(response.data.super_star_competition){
                     // $('.super_star').find('.owl-carousel').html(super_html);
                     if(response.data.super_star_competition.competition_media == 'video'){
                        $('.super_star').find('.owl-carousel').html(super_html);
                        // $('.super_star_img').parent().parent().hide();
                        // $('.super_audio').parent().parent().parent().hide();
                       }
                       if(response.data.super_star_competition.competition_media == 'image'){
                        $('.super_star_img').find('.owl-carousel').html(super_html);
                        // $('.super_star').parent().parent().parent().hide();
                        // $('.super_audio').parent().parent().parent().hide();
                       }

                     
                    }else{
                      $('.no_found__super_star').html(super_html);
                    }




                    dynamicCrousalClasses(d);

                    $('video, audio').mediaelementplayer({
                      pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
                      shimScriptAccess: 'always'
                    });
                    /*** end continal check ***/
                  }


                  if(response.data.status == 0){
                    $('.no_competition').show();
                    $('.pug_loader_premier').hide();
                    $('.pug_loader_popular').hide();
                    $('.pug_loader_rising_star').hide();
                    $('.pug_loader_winner_audio').hide();

                    $('.pug_loader_talent').hide();
                    $('.pug_loader_professional').hide();
                    $('.pug_loader_serious').hide();
                    $('.pug_loader_potential_star').hide();

                    $('.pug_loader_star_').hide();
                    $('.pug_loader_super_star').hide();
                    $('.no_found_all').html('No Competition Created Yet');
                    $('.audio_all').html('');
                    $('.premier_a').hide();
                    $('.winner_a').hide();
                    $('.popular_a').hide();
                    $('.rising_star_a').hide();
                    $('.talent_a').hide();
                    $('.professional_a').hide();
                    $('.serious_a').hide();
                    $('.potential_a').hide();
                    $('.star_r_a').hide();
                    $('.super_a').hide();
                  }
              })
              .catch(function (error) {
              })
            .finally(function () {
              // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
          });
          
    }
    $('.btn_judge_link').click(function(){
           window.location.href = $(this).data('url');
          });
     // });
    function videoHtml(key,item){
      var like = 0;
      $.each(item.user_likes,function(a,b){
        like++;
      });
        var a = `<div class="item vs-icon">
                       <img class="vs-img-position" src="../assets/images/vs-icon.png"/>
                          <div class="video-card mb-0">
                           
                            <p class="event-name">`+item.user.user_detail.name+`</p>
                            <p class="event-category">`+item.user_content.name+`</p>

                            <div class="player">
                              <button class="like-btn" onclick="likeMedia(`+key+`,`+item.id+`,`+item.user_id+`)">
                              <i class="fa fa-spinner fa-spin like_spinner like_spinner_`+key+`"></i>
                              <i class="fa fa-thumbs-up"></i> <span class="total_like_`+key+`">`+like+`</span>
                            </button>
                            <button class="btn_flag mar-right" onclick="report(` + key +`,` + item.id +`,` + item.user_id +`)">
                                <i class="fa fa-flag"></i>
                            </button>
                            <div class="abc"></div>
                            <video id="player1" width="640" height="360" preload="none" style="width: 100%; max-width: 100%; height: 231px" controls poster="{{config('constant.BUCKET_URL')}}`+item.user_content.thumbnail+`" playsinline webkit-playsinline>
                                <source src="{{config('constant.BUCKET_URL')}}`+item.user_content.file+`" type="video/mp4">
                              </video>
                            </div>

                          </div>
                        </div>`;
      return a;
    }

    function imageHtml(key,item){
      var like = 0;
      $.each(item.user_likes,function(a,b){
        like++;
      });
      var a = `<div class="item vs-icon">
                       <img class="vs-img-position" src="../assets/images/vs-icon.png"/>
                <div id="" class="img-card-parent overlay-dark">
                    <div class="img-card ">
                    <div class="abc"></div>
                      <img src="{{config('constant.BUCKET_URL')}}` + item.user_content.file +`" class="img-mini img-responsive" />
                    </div>
                    <button class="like-btn" onclick="likeMedia(` + key +`,` + item.id +`,` + item.user_id +`)">
                        <i class="fa fa-spinner fa-spin like_spinner like_spinner_` + key +`"></i>
                        <i class="fa fa-thumbs-up"></i> <span class="total_like_` + key +`">` + like +`</span>
                    </button>
                    <button class="btn_flag mar-right" onclick="report(` + key +`,` + item.id +`,` + item.user_id +`)">
                        <i class="fa fa-flag"></i>
                    </button>
                    
                        <p class="event-name">`+item.user.user_detail.name+`</p>
                        <p class="event-category">`+item.user_content.name+`</p>
                    <div class="abc">
                    </div>
                </div>

              </div>`;
      return a;
    }

   
    function audioHtml(key,item){
      var like = 0;
      $.each(item.user_likes,function(a,b){
        like++;
      });
      var a = ` <div class="item vs-icon">
                       <img class="vs-img-position" src="../assets/images/vs-icon.png"/>
                          <div id="waveform_`+key+`" class="audio-waveform wave">
                            <button class="like-btn" onclick="likeMedia(`+key+`,`+item.id+`,`+item.user_id+`)">
                              <i class="fa fa-spinner fa-spin like_spinner like_spinner_`+key+`"></i>
                              <i class="fa fa-thumbs-up"></i> <span class="total_like_`+key+`">`+like+`</span>
                            </button>
                            <button class="btn_flag mar-right" onclick="report(` + key +`,` + item.id +`,` + item.user_id +`)">
                                <i class="fa fa-flag"></i>
                            </button>
                            <div class="abc"></div>
                            <p class="event-name">`+item.user.user_detail.name+`</p>
                            <p class="event-category">`+item.user_content.name+`</p>
                            <div class="abc">
                                                    </div>
                              <img src="https://storage.googleapis.com/gong_bucket/uploads/image/1585216412audio.PNG" class="img-responsive img_`+key+`" style="position: absolute;overflow: hidden;" />
                              <div class="text-center">
                              <div class="spinner-border spinner_`+key+`" style="display:none; position:absolute"  role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                </div>
                            <button class="play-btn btn_play_`+key+`" type="button" name="playbtn" onclick="wavesurfer[`+key+`].playPause()">
                              <i class="fa fa-play"></i>

                            </button>
                            <button class="play-btn btn_load_`+key+`" type="button" name="playbtn" onclick="dynamicLoadData(wavesurfer[`+key+`],'{{config("constant.BUCKET_URL")}}`+item.user_content.file+`',`+key+`)">
                              <i class="fa fa-play"></i>
                            </button>
                          </div>
                          <div class="">

                          </div>
                        </div>`;


      return a;
    }

    function audioHandle(id){

            wavesurfer = WaveSurfer.create({
              container: "#waveform_"+id,
              waveColor: '#940000',
              progressColor: '#273043',
              responsive: true,
              // backend: 'MediaElement',
            });

            wavesurfer.load('../assets/audio/demo.wav');
            wavesurfer.on('ready', function () {
              let time = wavesurfer.getDuration();
              $("#duration").text(formateTime(time));
            });
            wavesurfer.on('audioprocess', function () {
              let time = wavesurfer.getCurrentTime();
              $("#currentDuration").text(formateTime(time));

            });
        }
         // var wavesurfer = Object.create(WaveSurfer);
    function renderWaveForm(url, parentSelector) {
      var domEl = document.createElement('div')
      document.querySelector(parentSelector).appendChild(domEl)

      var wavesurferr = WaveSurfer.create({
        container: domEl,
        waveColor: 'red',
        progressColor: 'purple',
        hideScrollbar: true,
        xhr: {
        cache: "default",
        mode: "cors",
        method: "GET",
        // credentials: "gong_bucket",
        headers: [
          { key: "cache-control", value: "no-cache" },
          { key: "Access-Control-Allow-Origin", value: "*" },
          { key: "Origin", value: "http://localhost:8000" },
          { key: "Origin", value: "*" },
          { key: "pragma", value: "no-cache" }
        ]
      }
      });
      // wavesurferr.load(url);
      return wavesurferr;
    }


    function formateTime(time) {
      var minutes = Math.floor(time / 60).toFixed(0);
      minutes = (minutes < 10)?"0"+minutes:minutes;
      var seconds = (time - minutes * 60).toFixed(0);
      seconds = (seconds < 10)?"0"+seconds:seconds;
      return minutes.substr(-2) + ":" + seconds;
    }
    function dynamicCrousalClasses(d){
      dunamicCrousal('.premier_audio',d);
      dunamicCrousal('.popular_audio',d);
      dunamicCrousal('.start_audio',d);
      dunamicCrousal('.winner_vid',d);
      dunamicCrousal('.got_talent_audio',d);
      dunamicCrousal('.professional_audio',d);
      dunamicCrousal('.serious_audio',d);
      dunamicCrousal('.potential_audio',d);
      dunamicCrousal('.star_audio',d);
      dunamicCrousal('.super_audio',d);

      /************** *****************/
      dunamicCrousal('.premier_img',d);
      dunamicCrousal('.winner_img',d);
      dunamicCrousal('.popular_img',d);
      dunamicCrousal('.rising_star_img',d);
      dunamicCrousal('.talent_img',d);
      dunamicCrousal('.professional_img',d);
      dunamicCrousal('.serious_talent_img',d);
      dunamicCrousal('.potential_img',d);
      dunamicCrousal('.star__img',d);
      dunamicCrousal('.super_star_img',d);

      dunamicCrousal('.premier_video',d);
      dunamicCrousal('.winner_audio',d);
      dunamicCrousal('.popular',d);
      dunamicCrousal('.rising_star',d);
      dunamicCrousal('.talent',d);
      dunamicCrousal('.professional',d);
      dunamicCrousal('.serious',d);
      dunamicCrousal('.potential_star',d);
      dunamicCrousal('.star_',d);
      dunamicCrousal('.super_star',d);
    }

    function dunamicCrousal(clas,d=0){
      // $(clas).find('.owl-carousel').owlCarousel(options);
      if(d == 0){
        $(clas).find('.owl-carousel').owlCarousel({
                        stagePadding: 40,
                        loop: false,
                        margin:18,
                        nav: true,
                        responsive:{
                          0:{
                            items:1
                          },
                          600:{
                            items: 1
                          },
                          768:{
                            items:2
                          }
                        }
                      });
      }else{
      // trigger('destroy.owl.carousel')
      $(clas).find('.owl-carousel').trigger('destroy.owl.carousel').owlCarousel({
                        stagePadding: 40,
                        loop: false,
                        margin:18,
                        nav: true,
                        responsive:{
                          0:{
                            items:1
                          },
                          600:{
                            items: 1
                          },
                          768:{
                            items:2
                          }

                        }
                      });
       
    }
       
    }

    function dynamicLoadData(wave,url,key){
      wave.load(url);
      wave.play();
      $('.btn_load_'+key).hide();
      $('.btn_play_'+key).show();
      $('.img_'+key).hide();
      $('.spinner_'+key).show();
      wave.on('ready', function () {
          $('.spinner_'+key).hide();
        });
      // wave.playPause();
      // $('id_'+key).remove();
    }

    function likeMedia(key,c_id,u_id){
      $('.like_spinner_'+key).show();
      axios({
              method: 'post',
              url: "{{route('user.content.add_like')}}",
              data:{'competition_user_id':c_id,'user_id':u_id},
              responseType: 'json'
            }).then(function (response) {
              $('.total_like_'+key).html(response.data.total_like);
            }).catch(function (error) {
          $('#signUpModal').modal("show");
      }).finally(function () {
              $('.like_spinner_'+key).hide();
          });
    }
    $(document).on('click','.play-btn',function(){
      $(this).find('i').toggleClass("fa-play fa-pause");
    });
   </script>
   @endsection


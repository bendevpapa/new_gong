
<div class="wrapper-pug">
    <div class="wrapper-cell-pug">

      <div>
        <div class="image-pug"></div>
        <div class="text-pug">
          <div class="text-line-pug"></div>
          <div class="text-line-pug"> </div>
        </div>
      </div>

  <div>
    <div class="image-pug"></div>
    <div class="text-pug">
      <div class="text-line-pug"></div>
      <div class="text-line-pug"></div>
    </div>
  </div>

</div>
  </div>

@section('css')
@parent
	<style>
body {
  background: #E9E9E9;
  flex-direction: row;
  display: flow-root;
  align-items: center;
  justify-content: center;
}
.wrapper-pug {
  padding: 30px;
  background: #fff;
  border-radius: 10px;
}
.wrapper-cell-pug {
  display: flex;
  justify-content: center;
}
@keyframes placeHolderShimmer{
  0%{
    background-position: -468px 0
  }
  100%{
    background-position: 468px 0
  }
}
.animated-background {
  animation-duration: 1.25s;
  animation-fill-mode: forwards;
  animation-iteration-count: infinite;
  animation-name: placeHolderShimmer;
  animation-timing-function: linear;
  background: #F6F6F6;
  background: linear-gradient(to right, #F6F6F6 8%, #F0F0F0 18%, #F6F6F6 33%);
  background-size: 800px 104px;
  height: 96px;
  position: relative;
  background: #F6F6F6;
}
.image-pug {
  height: 250px;
  width: 235px;
  margin-right: 10px;
  margin-left: 10px;
  margin-bottom: 10px;
  border-radius: 10px;
  @extend .animated-background;
   background: #F6F6F6;
}
.text-pug {
  display: flex;
}
.text-line-pug {
  height: 30px;
  width: 105px;
  border-radius: 4px;
  margin: 4px 10px;
  @extend .animated-background;
  background: #F6F6F6;
}


	</style>
@endsection

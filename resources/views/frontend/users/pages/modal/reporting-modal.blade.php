 
  <div class="modal fade" id="reportingModal" tabindex="-1" role="dialog" aria-labelledby="phoneVerification" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content theme-modal verification-modal" style="background: #940000">
        <div class="modal-header border-0 ">
          <button type="button" class="close px-2" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div class="container">
            <div class="row align-items-center justify-content-center">
              <div class="col-12 bg-red">
                <div class="row">
                  <div class="col-12 text-center">
                    <h3 class="text-white mb-4">Please select a problem below</h3>
                  </div>
                </div>
                <div class="row">
                  <div class="report_server_response col-12">
                  </div>
                </div>
                <div class="row">
                    @if(@$profanity)
	                    @foreach($profanity as $key => $value)
		                  <div class="col-md-4 mb-4 text-center">
		                    <button type="button" class="btn btn-primary rounded-pill report {{($key == 0) ? 'btn_reporting' : ''}}" data-value="{{$value->id}}">
		                      {{ucFirst($value->description)}}
		                    </button>
                        <input type="hidden"  name="report_id" class="report_hidden" value="{{($key == 0) ? $value->id : ''}}">
		                    <input type="hidden"  name="user_content_id" class="user_content_id">
		                  </div>
	                  	@endforeach
                  	@endif
                  
                  <div class="col-md-2"></div>
                </div>
                <div class="border-bottom mb-4"></div>
                <div class="row">
                  <div class="col-md-12 mb-4 text-center">
                    <button type="submit" class="btn btn-primary rounded-pill btn_report_send btn_reporting">
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
@section('script')
@parent
<script type="text/javascript">
	function report(key,id,user_id){
        $( "#reportingModal").modal({
            backdrop: 'static',
            keyboard: false
        });
        $('.user_content_id').val(id);
     }
     $('.report').click(function(){
     	$('.report').removeClass('btn_reporting');
     	$(this).addClass('btn_reporting');
     	$('.report_hidden').val($(this).data('value'));
     })
     $('.btn_report_send').click(function(){
        $('.btn_report_send').html(loader());
        $('.btn_report_send').attr('disabled',true);

     	  // $.ajax({
        //       url: "{{route('user.report.save')}}",
        //       method: 'post',
        //       data: {'report_id':$('.report_hidden').val(),'user_content_id':$('.user_content_id').val()},
        //       dataType: "json",
        //       success: function(response){
        //         $('.btn_report_send').html('Submit');
        //         $('.btn_report_send').attr('disabled',false);
        //         if (response.status == 1){
        //             var ht = '<ul>';
        //             ht += "<li class='alert alert-success'>"+response.message+"</li>";
        //             setTimeout(function(){ $(".report_server_response").html(''); }, 3000);
        //             $(".report_server_response").html(ht);
        //         }
        //         if (response.status == 0){
        //             var ht = '<ul>';
        //               ht += "<li class='alert alert-danger'>"+response.message+"</li>";
        //              setTimeout(function(){ $(".report_server_response").html(''); }, 3000);
        //             $(".report_server_response").html(ht);
        //         }
        //       }
        // });
        axios({
              method: 'post',
              url: "{{route('user.report.save')}}",
              data:{'report_id':$('.report_hidden').val(),'user_content_id':$('.user_content_id').val()},
              responseType: 'json'
            }).then(function (response) {
              $('.btn_report_send').html('Submit');
                $('.btn_report_send').attr('disabled',false);
                if (response.data.status == 1){
                    var ht = '<ul>';
                    ht += "<li class='alert alert-success'>"+response.data.message+"</li>";
                    setTimeout(function(){ $(".report_server_response").html(''); }, 3000);
                    $(".report_server_response").html(ht);
                }
                if (response.data.status == 0){
                    var ht = '<ul>';
                      ht += "<li class='alert alert-danger'>"+response.data.message+"</li>";
                     setTimeout(function(){ $(".report_server_response").html(''); }, 3000);
                    $(".report_server_response").html(ht);
                }
            }).catch(function (error) {
              $('.btn_report_send').html('Submit');
              $('.btn_report_send').attr('disabled',false);
              $('#signUpModal').modal("show"); 
              $('#reportingModal').modal("hide");
      }).finally(function () {
              // $('.like_spinner_'+key).hide();
      });

     })
</script>
@endsection
 
<div class="tab-pane fade show active" id="nav-Social" role="tabpanel" aria-labelledby="nav-Social-tab">
                    <div class="" id="submit-for-competition-1">
                        <div class="competition-title align-items-end mb-4 d-flex justify-content-between">
                            <h2>
                                Submit for Competition
                            </h2>
                            <p>Step 1/2</p>
                        </div>
                        <form id="contentForm" action="#" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                            <label for="Name" style="color: #940000 !important; font-size: 14px;font-weight: 500;">Choose a Category</label>
                            <div class="category-btn category-align border-bottom mb-4">
                                @if($data['user']['mediaTypes'])

                                    @foreach($data['user']['mediaTypes'] as $key => $item)
                                        @if($key == 0)
                                            <input type="hidden" value="{{$item->id}}" name="category_id" class="category_id">
                                        @endif
                                        <button type="button" class="button-wrapper  file-type category-lead btn_media_type   {{ ($key == 0) ?  'active' : ''}}"
                                                data-valuee="{{$item->id}}" data-value="{{($item->name_key == 'photo-modeling') ? 'image' : 'video'}}"
                                                data-id="{{$item->id}}" @if($item->name_key == 'photo-modeling')
                                                data-accept="image/*" @else data-accept="video/*" @endif>
                                         <span class="label">
                                            {!! $item->avatar !!}
                                             {{-- <i class="fa fa-microphone"></i> --}}
                                         </span>
                                            <label class="c-name" for="" >{{$item->name}}</label>
                                        </button>
                                    @endforeach
                                @endif
                            </div>
                            <div class="file-upload upload-btn-group text-center ">
                                <input type="hidden" name="file_type" value="video">
                               <!--  <div class="button-wrapper file-type active" data-value="video" data-accept="video/*">
                                   <span class="label">
                                      <i class="fa fa-video-camera"></i>
                                   </span>
                                   <p>Video</p>
                                </div>
                                <div class="button-wrapper file-type" data-value="audio" data-accept="audio/*">
                                   <span class="label">
                                      <i class="fa fa-music"></i>
                                   </span>
                                   <p>Audio</p>
                                </div>
                                <div class="button-wrapper file-type" data-value="image" data-accept="image/*">
                                  <span class="label">
                                    <i class="fa fa-image"></i>
                                  </span>
                                  <p>Image</p>
                                </div> -->
                                <div class="row justify-content-center">
                                    <div class="alert alert-danger error_msg" role="alert">
                                    </div>
                                </div>
                                <div class="row">
                                    {{-- for Dropdown --}}
                                   <div class="col-md-4">
                                        <div class="input-field">
                                            <label for="Genre" style="color: #940000 !important;">Choose a Genre</label>
                                            <div class="input-group mb-4">
                                                <div class="input-pre-icon">
                                                    <img src="../assets/images/genre-icon.png" alt="genre-icon" class="input-icon">
                                                </div>
                                                <select class="form-control genre_id" name="genre_id" id="Genre">
                                                    <option value="" selected="">Please choose genre</option>
                                                    @if($data['user']['genres'])
                                                        @foreach($data['user']['genres'] as $key => $item)
                                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <em class="genre_id-error-section" style="display: none;">Please select genre</em>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- for Dropdown End --}}
                                </div>
                                <div class="tab-content border-bottom mb-5 mt-4" >
                                    <div class="upload-preview position-relative">
                                        <label>
                                            <img src="{{asset('assets/images/upload-placeholder.png')}}" class="placeholder_media" alt="">
                                            <input name="file" type="file" id="file" accept="video/*"/>

                                        </label>

                                        <div class="video-preview-container file-preview-container" style="display: none;">
                                            <video class="video-preview video-preview-nn_bb" id="player1" preload="none" style="width: 100%; max-width: 100%; height: auto" autoplay controls>
                                            </video>

                                        </div>
                                        <input type="hidden" name="img_thum" class="img_thum">
                                            <canvas id="canvas-element" style="display: none;"></canvas>

                                        <div class="audio-preview-container file-preview-container" style="display: none;">
                                            <img class="audio-preview-img" src="" id="" preload="none" style="width: 100%; max-width: 100%;">
                                            <audio class="audio-preview" id="player1" preload="none" autoplay controls>

                                            </audio>
                                        </div>
                                        <div class="img-preview-container file-preview-container" style="display: none;">
                                            <img class="img-preview" src="" id="player1" preload="none" style="width: 100%; max-width: 100%;">
                                        </div>
                                        <div class="afg mt-2" onclick="refreshMedia()" data-value="image" data-accept="image/*">
                                              <span class="label">
                                                <i  style="font-style: normal;" class="btn btn-primary rounded-pill">Upload</i>
                                              </span>
                                        </div>
                                    </div>

                                    <div>
                                        <em class="file-error-section" style="display: none;">Please provide valid file</em>
                                    </div>
                                </div>
                            </div>


                            <center class="loader_video">
                                <div class="spinner-border text-warning" role="status">
                                  <span class="sr-only">Loading...</span>
                                </div>
                            </center>
                            <div class="theme-form ">
                                <div class="container">
                                    <div class="row border-bottom mb-4">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <div class="input-field">
                                                <label for="Name" style="color: #940000 !important;">Give it a Name</label>
                                                <div class="input-group mb-4">
                                                    <div class="input-pre-icon">
                                                        <img src="../assets/images/cometition-icon.png" alt="cometition-icon" class="input-icon">
                                                    </div>
                                                    <input type="text" class="form-control" name="name" id="Name">
                                                    <em class="name-error-section" style="display: none;">Please provide name</em>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-12 d-flex justify-content-center">
                                            <div class="mb-5">
                                                <div class="checkbox checkbox-primary checkbox-inline">
                                                    <input type="checkbox" id="inlineCheckbox2" value="option1" checked="" class="privacy-check">
                                                    <label for="inlineCheckbox2"> I agree to the <a href="{{route('privacy')}}"> <strong>Privacy Policy</strong> </a> </label>
                                                    <em style="display: none;">Please agree to privacy policy</em>
                                                </div>
                                                <div class="checkbox checkbox-primary checkbox-inline">
                                                    <input type="checkbox" id="inlineCheckbox1" checked="" value="option1" class="terms-check">
                                                    <label for="inlineCheckbox1"> I agree to the <a href="{{route('term-condition')}}"> <strong>Terms & Conditions</strong> </a> </label>
                                                    <em style="display: none;">Please agree to terms & condition</em>
                                                </div>
                                                <input type="hidden" name="category_selecred" class="category_selecred" value="vocals">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-5 text-center">
                                            <button type="button" class="btn btn-primary rounded-pill" id="buttonProceed">Proceed</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="d-none" id="submit-for-competition-2">
                        <div class="competition-title align-items-end mb-4 d-flex justify-content-between">
                            <h2>
                                Submit for Competition
                            </h2>
                            <p>Step 2/2</p>
                        </div>
                        <div class="container">
                            <div class="row ">
                                <div class="col-12">
                                    <div class="progress my_progress mb-4">
                                      <div class="progress-bar myprogress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <center class="loader_progess">
                                        <div class="spinner-border text-warning" role="status">
                                          <span class="sr-only">Loading...</span> Wait ...
                                        </div>
                                    </center>
                                    <div class="text-center">
                                        <div class="pb-4">
                                            <button class="btn btn-primary rounded-pill"  id="submitPublicCompetition">Gong Competition</button>
                                        </div>

                                        <div class="pb-5 border-bottom">
                                            <button class="btn btn-primary rounded-pill" id="privateCompetition">Private Competition</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="submit-for-competition-3" style="display: none;">
                        <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
                            <h2>
                                Submission Successful
                            </h2>
                        </div>
                        <p class="title-lead">
                            <!-- Your <span class="file_type_show"></span> has been submitted for Public competition. You will be notified when the public competition starts.
                            back to <a href="{{route('user.dashboard')}}">Dashboard </a> -->

                            Congratulations, Your <span class="file_type_show"></span> has been submitted for <span class="file_sub_com"></span>. You will be notified when your competition begins and receive a link that you can share with your friends. Verify your number below so we can send you a text when your competition starts.
                        </p>
                        @if(Auth::user()->phone_status == 'unverified')
	                        <div class="verify-wrapper phone-screen-1">
							    <div class="dialog">
							        <button class="close btn_ph_sc_1">×</button>
							        <h3>Please Verify Phone Number</h3>
                                    <div class="sc_1_error"></div>
							        <div id="form2" class="text-center">
							            <input class="form-control mb-3" readonly="" value="{{Auth::user()->phone}}" type="text" />
							            <button class="btn btn-primary btn-embossed btn_ph_verified">Verify</button>
							        </div>
                                     <div>
                                        Didn't receive the code?<br />
                                        <!-- <a href="javascript:;" class="send_code_again">Send code again</a><br /> -->
                                        <a href="javascript:;" class="change_number_1">Change phone number</a>
                                    </div>

							    </div>
							</div>
	                        <div class="verify-wrapper hide-display	 phone-screen-2">
							    <div class="dialog">
							        <button class="close btn_ph_sc_2">×</button>
							        <h3>Please enter the 4-digit verification code we sent via SMS:</h3>
							        <div class="error_sc2"></div>
							        <span>(we want to make sure it's you before we contact our movers)</span>
							        <div id="form">
							            <input type="text" name="code_name1" class="code_name1" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
							            <input type="text" name="code_name2" class="code_name2" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" /><input type="text" name="code_name3" class="code_name3" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" /><input type="text" name="code_name4" class="code_name4" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
							            <button class="btn btn-primary btn_verify_code btn-embossed">Verify</button>
							        </div>

							        <div>
							            Didn't receive the code?<br />
							            <a href="javascript:;" class="send_code_again">Send code again</a><br />
							            <a href="javascript:;" class="change_number">Change phone number</a>
							        </div>
							    </div>
							</div>
							<div class="verify-wrapper dsf hide-display phone-screen-3">
							    <div class="dialog">
							        <button class="close btn_ph_sc_3">×</button>
							        <h3>Change Phone Number</h3>
							        <div id="form2" class="text-center">
							        	<form method="post" action="javascript:;" id="change_number_form">
							           <div class="input-field">

	                                        <label for="Phone">Phone*</label>

	                                        <div class="input-group mb-3">
	                                            <input type="tel" autocomplete="off" name="phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"  class="form-control telephone_v tel_new_number" id="Phone">

	                                        </div>
	                                        <div class="err_em hide-display">
	                                        	Please Enter Phone Number
	                                        </div>
	                                    </div>
							            <button class="btn btn-primary tel_new_number_btn btn-embossed">Confirm</button>
							        </div>

							    </div>
							</div>
						@endif
                    </div>
                    <div id="submit-for-competition-4" style="display: none;">
                        <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
                            <h2>
                                Submission Successful
                            </h2>
                        </div>
                        <p class="title-lead">
                            Your <span class="file_type_show"></span> has been submitted for <span class="competition_type_a"></span>. You will be notified when the <span class="competition_type_a"></span> starts.
                        </p>
                    </div>
                    <div class="row private-competition-1" style="display: none;">
                      <div class="col-12">
                        <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
                          <h2>
                            Join a Private Competition
                          </h2>
                        </div>
                        <p class="title-lead">
                          Input the private competition code below and you will be notified if your submission is placed in the comptition.
                        </p>
                        <div class="error_code_private">
                        </div>
                        <div class="container">
                          <div class="row pb-4 mb-5">
                            <div class="col-12 col-sm-4 offset-md-4 align-self-center">
                              <form action="" class="theme-form">
                                <div class="input-field">
                                  <label for="code" class="text-dark">Code</label>
                                  <div class="input-group mb-4">
                                    <input type="text" name="code" class="form-control border-dark" id="code">
                                  </div>
                                </div>
                                <div class="text-center">
                                  <a class="py-2 btn btn-primary rounded-pill btn_private_code" href="javascript:;">Submit</a>
                                </div>
                              </form>
                            </div>
                            <div class="col-12 my-4">
                              <p class="border-with-text fancy"><span>OR</span></p>
                            </div>
                            <div class="col-12">
                              <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
                                <h2>
                                  Join an Existing Competition
                                </h2>
                              </div>
                            </div>
                            <div class="container">
                              <div class="row public_competition">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>




@if(Auth::user()->phone_status == 'unverified')
<!-- <input type="hidden" class="telephone"> -->
	@section('script')
	@parent

	<script type="text/javascript">
	/*************************      Phone verified Javascript ************/
	    $(function() {
	        'use strict';

	        var body = $('body');

	        function goToNextInput(e) {
	            var key = e.which,
	                t = $(e.target),
	                sib = t.next('input');

	            if (key != 9 && (key < 48 || key > 57)) {
	                e.preventDefault();
	                return false;
	            }

	            if (key === 9) {
	                return true;
	            }

	            if (!sib || !sib.length) {
	                sib = body.find('input').eq(0);
	            }
	            sib.select().focus();
	        }

	        function onKeyDown(e) {
	            var key = e.which;

	            if (key === 9 || (key >= 48 && key <= 57)) {
	                return true;
	            }

	            e.preventDefault();
	            return false;
	        }

	        function onFocus(e) {
	            $(e.target).select();
	        }

	        // body.on('keyup', '#form input', goToNextInput);
	        // body.on('keydown', '#form input', onKeyDown);
	        // body.on('click', '#form input', onFocus);

	    })
	    var input = document.querySelector(".telephone_v");
	                    window.intlTelInput(input, {
	                      // allowDropdown: false,
	                      // autoHideDialCode: false,
	                      // autoPlaceholder: "off",
	                      dropdownContainer: document.body,
	                      // excludeCountries: ["us"],
	                      formatOnDisplay: true,
	                      geoIpLookup: function(callback) {
	                        $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
	                          var countryCode = (resp && resp.country) ? resp.country : "";
	                          callback(countryCode);
	                        });
	                      },
	                      hiddenInput: "full_number",
	                      initialCountry: "",
	                      // localizedCountries: { 'de': 'Deutschland' },
	                      // nationalMode: false,
	                      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
	                      // placeholderNumberType: "MOBILE",
	                      // preferredCountries: ['cn', 'jp'],
	                      separateDialCode: true,
	                      utilsScript: "{{asset('assets/js/utils.js')}}",
	                });

	    $('.btn_ph_sc_1').click(function(){
	    	$('.phone-screen-1').addClass('hide-display');
	    });
	    $('.btn_ph_sc_2').click(function(){
	    	$('.phone-screen-2').addClass('hide-display');
	    });
	    $('.btn_ph_sc_3').click(function(){
	    	$('.phone-screen-3').addClass('hide-display');
	    });
	    $('.btn_ph_verified').click(function(){
	    	$(this).html(loader());
	    	$.ajax({
	            url: "{{route('user.phone.send-verify')}}",
	            type: 'post',
	            dataType:'json',
	            success: function ( response ) {
	               if(response.status == 1){
	                $('.btn_ph_verified').html('Verify');
	                $('.phone-screen-1').addClass('hide-display');
	                $('.phone-screen-2').removeClass('hide-display');
	               }
	            },
	            error: function (errors) {
                    console.log(errors);
                    $('.sc_1_error').html('<ul><li class="alert alert-danger">'+errors.responseJSON.message+'</li></ul>')
	            	$('.btn_ph_verified').html('Verify');
                    setTimeout(function(){ $(".sc_1_error").html(''); }, 3000);
	            }
	        });
	    });
        $('.send_code_again').click(function(){
            $(this).html(loader());
            $.ajax({
                url: "{{route('user.phone.send-verify')}}",
                type: 'post',
                dataType:'json',
                success: function ( response ) {
                   if(response.status == 1){
                    $('.error_sc2').html('<ul><li class="alert alert-success">Code Send in your phone number</li></ul>');
                    setTimeout(function(){ $(".error_sc2").html(''); }, 3000);
                     $('.send_code_again').html('Send code again');
                   }
                },
                error: function (errors) {
                    $('.error_sc2').html('<ul><li class="alert alert-danger">'+errors.responseJSON.message+'</li></ul>')

                    setTimeout(function(){ $(".error_sc2").html(''); }, 3000);
                    $('.send_code_again').html('Send code again');
                    // $('.send_code_again').html('Verify');
                }
            });
        });
	    $('.change_number').click(function(){
	    	$('.phone-screen-2').addClass('hide-display');
	    	$('.phone-screen-3').removeClass('hide-display');
	    });
        $('.change_number_1').click(function(){
            $('.phone-screen-1').addClass('hide-display');
            $('.phone-screen-3').removeClass('hide-display');
        });
	    $('.tel_new_number_btn').click(function(){
	    	if($('.tel_new_number').val() ==  ""){
	    		$('.err_em').removeClass('hide-display');
	    		return false;
	    	}
	    	var n = $('.iti__selected-dial-code').html();
	    	n+= $('.tel_new_number').val();
	    	$('.tel_new_number_btn').html(loader());
	    	$.ajax({
	            url: "{{route('user.phone.change_number')}}",
	            type: 'post',
	            data : {'number' : n},
	            dataType:'json',
	            success: function ( response ) {
	               if(response.status == 1){
	                $('.tel_new_number_btn').html('Verify');
	                $('.phone-screen-1').addClass('hide-display');
	                $('.phone-screen-3').addClass('hide-display');
	                $('.phone-screen-2').removeClass('hide-display');
	               }
	            },
	            error: function (errors) {

	            	$('.tel_new_number_btn').html('Verify');
	            }
	        });
	    });
	    $('.btn_verify_code').click(function(){
	    	$(this).html(loader());
	    	var a = $('.code_name1').val() + $('.code_name2').val() + $('.code_name3').val() + $('.code_name4').val();

	    	$.ajax({
	            url: "{{route('user.phone.verify')}}",
	            type: 'post',
	            data:{'code':a},
	            dataType:'json',
	            success: function ( response ) {
	               if(response.status == 1){
	                $('.btn_verify_code').html('Verify');
	                $('.phone-screen-1').addClass('hide-display');
	                $('.phone-screen-2').removeClass('hide-display');
	               	$('.error_sc2').html('<ul><li class="alert alert-success">Phone Verification is completed</li></ul>');
	               	setTimeout(function(){ $(".error_sc2").html(''); }, 3000);
	               	location.reload();
	               }
	               if(response.status == 0){
	               	$('.error_sc2').html('<ul><li class="alert alert-danger">Your code is Invalid</li></ul>');
	               	setTimeout(function(){ $(".error_sc2").html(''); }, 3000);
	               	$('.btn_verify_code').html('Verify');
	               }
	            },
	            error: function (errors) {
                    $('.error_sc2').html('<ul><li class="alert alert-danger">'+errors.responseJSON.message+'</li></ul>')

                    setTimeout(function(){ $(".error_sc2").html(''); }, 3000);
	            	$('.btn_verify_code').html('Verify');
	            }
	        });
	    });
	</script>
	@endsection
@endif

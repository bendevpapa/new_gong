@extends('frontend.users.layout.dashboard-master')

@section('css')
    @parent
    <link rel="stylesheet" href="{{asset('assets/css/dropzone.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/video/mediaelementplayer.min.css')}}">
@endsection

@section('dash-content')
<div class="competition-from">
        <nav class="form-steps-nav">
            <div class="nav nav-tabs" id="competitor-nav-tab" role="tablist">
                <a class="nav-item nav-link" href="{{route('user.dashboard')}}" >Competitions</a>
                 <a class="nav-item nav-link" href="{{route('user.dashboard')}}" >Competition Detail</a> 
            </div>
        </nav>
    <div class="container">
	    <div class="row">
	      <div class="col-12">
	        <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
	          <h2>
	            Join a Private Competition
	          </h2>
	        </div>
	        <p class="title-lead">
	          Input the private competition code below and you will be notified if your submission is placed in the comptition.
	        </p>
	        <div class="container">
	          <div class="row pb-4 mb-5">
	            <div class="col-12 col-sm-4 offset-md-4 align-self-center">
	              <form action="" class="theme-form">
	                <div class="input-field">
	                  <label for="code" class="text-dark">Code</label>
	                  <div class="input-group mb-4">
	                    <input type="text" class="form-control border-dark" id="code">
	                  </div>
	                </div>
	                <div class="text-center">
	                  <a class="py-2 btn btn-primary rounded-pill" href="javascript:;">Submit</a>
	                </div>
	              </form>
	            </div>
	            <div class="col-12 my-4">
	              <p class="border-with-text fancy"><span>OR</span></p>
	            </div>
	            <div class="col-12">
	              <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
	                <h2>
	                  Join an Existing Competition
	                </h2>
	              </div>
	            </div>
	            <div class="container">
	              <div class="row">
	                <div class="col-md-6 mb-4">
	                  <div class="join-list" data-toggle="modal" data-target="#join-list-modal">
	                    <div class="d-flex justify-content-between align-items-center">
	                      <h4>Creative Competition</h4>
	                      <i class="fa fa-microphone"></i>
	                    </div>
	                    <table class="table">
	                      <tr>
	                        <td>
	                          <i class="fa fa-list-ol"></i>
	                          <span>2Round</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-user"></i>
	                          <span>8 Participants</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-clock-o"></i>
	                          <span>24 hours</span>
	                        </td>
	                      </tr>
	                      <tr>
	                        <td>
	                          <i class="fa fa-user-plus"></i>
	                          <span>Duet</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-user-times"></i>
	                          <span>Elimination</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-clock-o"></i>
	                          <span>2 days</span>
	                        </td>
	                      </tr>
	                    </table>
	                  </div>
	                </div>
	                <div class="col-md-6 mb-4">
	                  <div class="join-list" data-toggle="modal" data-target="#join-list-modal">
	                    <div class="d-flex justify-content-between align-items-center">
	                      <h4>Creative Competition</h4>
	                      <i class="fa fa-microphone"></i>
	                    </div>
	                    <table class="table">
	                      <tr>
	                        <td>
	                          <i class="fa fa-list-ol"></i>
	                          <span>2Round</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-user"></i>
	                          <span>8 Participants</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-clock-o"></i>
	                          <span>24 hours</span>
	                        </td>
	                      </tr>
	                      <tr>
	                        <td>
	                          <i class="fa fa-user-plus"></i>
	                          <span>Duet</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-user-times"></i>
	                          <span>Elimination</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-clock-o"></i>
	                          <span>2 days</span>
	                        </td>
	                      </tr>
	                    </table>
	                  </div>
	                </div>
	                <div class="col-md-6 mb-4">
	                  <div class="join-list" data-toggle="modal" data-target="#join-list-modal">
	                    <div class="d-flex justify-content-between align-items-center">
	                      <h4>Creative Competition</h4>
	                      <i class="fa fa-microphone"></i>
	                    </div>
	                    <table class="table">
	                      <tr>
	                        <td>
	                          <i class="fa fa-list-ol"></i>
	                          <span>2Round</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-user"></i>
	                          <span>8 Participants</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-clock-o"></i>
	                          <span>24 hours</span>
	                        </td>
	                      </tr>
	                      <tr>
	                        <td>
	                          <i class="fa fa-user-plus"></i>
	                          <span>Duet</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-user-times"></i>
	                          <span>Elimination</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-clock-o"></i>
	                          <span>2 days</span>
	                        </td>
	                      </tr>
	                    </table>
	                  </div>
	                </div>
	                <div class="col-md-6 mb-4">
	                  <div class="join-list" data-toggle="modal" data-target="#join-list-modal">
	                    <div class="d-flex justify-content-between align-items-center">
	                      <h4>Creative Competition</h4>
	                      <i class="fa fa-microphone"></i>
	                    </div>
	                    <table class="table">
	                      <tr>
	                        <td>
	                          <i class="fa fa-list-ol"></i>
	                          <span>2Round</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-user"></i>
	                          <span>8 Participants</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-clock-o"></i>
	                          <span>24 hours</span>
	                        </td>
	                      </tr>
	                      <tr>
	                        <td>
	                          <i class="fa fa-user-plus"></i>
	                          <span>Duet</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-user-times"></i>
	                          <span>Elimination</span>
	                        </td>
	                        <td>
	                          <i class="fa fa-clock-o"></i>
	                          <span>2 days</span>
	                        </td>
	                      </tr>
	                    </table>
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
</div>
@endsection
@include('frontend.users.js-css-blades.select2')
@include('frontend.users.js-css-blades.form-validation')
@section('script')
    @parent
 @endsection

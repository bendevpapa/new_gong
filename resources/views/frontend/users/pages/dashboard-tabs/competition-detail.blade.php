<div class="competition-tab">
    <div class="mb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-lg-inline d-md-none"></div>
                <div class="col-lg-6 col-md-12">
                    <div class="d-flex">
                        <div class="input-group mr-2">
                            <select name="" id="Round1" class="form-control">
                                <option value="">Sort by: name</option>
                                <option value="">abc</option>
                                <option value="">abc</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <select name="" id="Round1" class="form-control ">
                                <option value="">Filter by: none</option>
                                <option value="">Round1</option>
                                <option value="">Round1</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="mb-3">
	  <div class="red-title-bg">
	    <div class="d-flex justify-content-sm-between justify-content-center align-items-center">
	      <h2 class="m-0">Active Competitions <span> <i class="fa fa-info-circle"></i> </span></h2>
	      <a href="">View all</a>
	    </div>
	  </div>

	  <div class="active-competition-videos">
	    <div class=" active_competiton">
	      <!-- <div class="col-md-4"> -->
	      	<div class="row active_img_detail_page avc"></div>
	      	<!-- <div class="active_vid_detail_page avc"></div>
	      	<div class="active_audio_detail_page avc"></div> -->
	        <!-- <div class="video-card">
	          <img src="../assets/images/premier-placeholder.png" alt="">
	        </div> -->
	      <!-- </div> -->

	    </div>
	    <div class="row active_no_competiton hide-display">
          <div class="col-12 my-5 text-center">
            <h1 class="coming-soon-title">
              No Competition
            </h1>
          </div>
        </div>
        <center class="active_competition_loader">
            <div class="spinner-border text-warning" role="status">
              <span class="sr-only">Loading...</span>
            </div>
        </center>
	  </div>
	</div>
	<div class="mb-3">
	  <div class="gray-title-bg">
	    <div class="d-flex justify-content-sm-between justify-content-center align-items-center">
	      <h2 class="m-0">Past Competitions <span><i class="fa fa-info-circle"></i></span> </h2>
	      <a href="">View all</a>
	    </div>
	  </div>
	  <div class="active-competition-videos">
	    <div class="  inactive_competiton">
	      <!-- <div class="col-md-4"> -->
	        <!-- <div class="video-card">
	          <img src="../assets/images/premier-placeholder.png" alt="">
	        </div>
	        <div class=" row inactive_img_detail_page avc"></div>
	      	<div class="inactive_vid_detail_page avc"></div>
	      	<div class="inactive_audio_detail_page avc"></div> -->
	      <!-- </div> -->
	    </div>
	    <div class="row inactive_no_competiton hide-display">
          <div class="col-12 my-5 text-center">
            <h1 class="coming-soon-title">
              No Competition
            </h1>
          </div>
        </div>
        <center class="inactive_competition_loader">
            <div class="spinner-border text-warning" role="status">
              <span class="sr-only">Loading...</span>
            </div>
        </center>
	  </div>
	</div>

{{--  @role('fan')--}}
  <div class="mb-3">
    <div class="gray-title-bg">
      <div class="d-flex justify-content-sm-between justify-content-center align-items-center">
        <h2 class="m-0">Pending Competitions <span><i class="fa fa-info-circle"></i></span> </h2>
        <a href="">View all</a>
      </div>
    </div>
    <div class="active-competition-videos">
      <div class="  onhold_competiton">
          <div class=" ">
            <div class="pd-x-15 pd-b-15">
              <div class="table-wrapper">
                  <table class="table table-striped datatable">
                      <thead>
                         <tr>
                            <th>Name</th>
                            <th>Create Date</th>
                            <th>Action</th>

                         </tr>
                      </thead>
                      <tbody>

                      </tbody>
                   </table>
                </div>
            </div>
          </div>
      </div>
        <center class="onhold_competition_loader">
            <div class="spinner-border text-warning" role="status">
              <span class="sr-only">Loading...</span>
            </div>
        </center>
    </div>
  </div>
{{--  @endrole--}}
</div>

@section('script')
@parent
<script type="text/javascript">
	var wavesurfer = [];
  var oTable = '';
    $(document).ready(function()
    {
           var oTable =   $('.datatable').DataTable({

            responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
                sProcessing: "<i class='fa fa-spinner fa-spin'></i>",
             },

             processing: true,
             serverSide: true,
             deferRender: true,
             order: [[ 1, 'asc' ]],
             "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
             "ajax": {
                'data': function (d) {
                d.name = $('input[type=search]').val();
            },
             'url': '{{ url('user/competitions') }}',
             'type': 'POST',
             'headers': {
             'X-CSRF-TOKEN': '{{ csrf_token() }}'
           }
           },

           columns: [
               {data: 'competition_name', name: 'competition_name'},
               {data: 'date', name: 'date'},
               {data: 'delete', name: 'delete'},


           ]
       });
     $('.dataTables_length select').addClass( "select2" );
       });
	function getUserCompetition(){
		$('.avc').html('');
		$('video').trigger('pause');
        $('audio').trigger('pause');
		    $('.active_competition_loader').show();
        $('.inactive_competition_loader').show();
        $('.onhold_competition_loader').show();
        $('.active_no_competiton').addClass('hide-display');
        $('.inactive_no_competiton').addClass('hide-display');
        $('.onhold_no_competiton').addClass('hide-display');
	 axios({
            method: 'get',
            url: "{{route('user.content.get-user-competition')}}",
            responseType: 'json'
        }).then(function (response) {
        	$('.active_competition_loader').hide();
          $('.inactive_competition_loader').hide();
        	$('.onhold_competition_loader').hide();
        		if(response.data.active_competiton_status == 1){
        			var active_html = '';
        			$.each(response.data.active_competiton,function(key,item){
	                   if(item.competition_media == 'video'){
	                   	if(item.single_competition_detail){
	                   		active_html += videoHtml(item);
	                   	}
	                   	// $('.active_img_detail_page').append(active_html);
	                   }
	                   if(item.competition_media == 'audio'){
	                   	if(item.single_competition_detail){
	                   		active_html += audioHtml(item,key);
	                   		// wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.single_competition_detail.user_content.file, '#waveform_'+key);
	                   	}
	                   	// $('.active_img_detail_page').append(active_html);
	                   }
	                   if(item.competition_media == 'image'){
	                   	if(item.single_competition_detail){
	                   		active_html += imgHtml(item);
	                   	}
	                   	// $('.active_img_detail_page').append(active_html);
	                   }
	                });
        		}else{
        			$('.active_no_competiton').removeClass('hide-display');
        		}
        		$('.active_img_detail_page').html(active_html);
        		if(response.data.inactive_competiton_status == 1){
        			var inactive_html = '';
        			$.each(response.data.inactive_competiton,function(key,item){
	                   if(item.competition_media == 'video'){
                      if(item.single_competition_detail){
	                   	inactive_html += videoHtml(item);
                     }
	                   }
	                   if(item.competition_media == 'audio'){
                      if(item.single_competition_detail){
  	                   	inactive_html += audioHtml(item,key);
                       }
	                   }
	                   if(item.competition_media == 'image'){
                      if(item.single_competition_detail){
  	                   	inactive_html += imgHtml(item);
                       }
	                   }
	                });
        		}else{
        			$('.inactive_no_competiton').removeClass('hide-display');
        		}
            if(response.data.hold_competition_status == 1){
              var hold_html = '';
              $.each(response.data.hold_competiton,function(key,item){
                     if(item.competition_media == 'video'){
                      if(item.single_competition_detail){
                      hold_html += videoHtml(item);
                     }
                     }
                     if(item.competition_media == 'audio'){
                      if(item.single_competition_detail){
                        hold_html += audioHtml(item,key);
                       }
                     }
                     if(item.competition_media == 'image'){
                      if(item.single_competition_detail){
                        hold_html += imgHtml(item);
                       }
                     }
                  });
                $('.onhold_img_detail_page').html(hold_html);
            }else{
              console.log(123);
              $('.onhold_no_competiton').removeClass('hide-display');
            }

              $('.inactive_img_detail_page').html(inactive_html);

        		$('.video_class_detail').mediaelementplayer({
		            // Do not forget to put a final slash (/)
		            pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
		            // this will allow the CDN to use Flash without restrictions
		            // (by default, this is set as `sameDomain`)
		            shimScriptAccess: 'always'
		            // more configuration
		        });

        }).catch(function (error) {
        }).finally(function () {
        });
    }
    function videoHtml(item){
    	var a = `<div class="col-md-4" style="float:left">
    						<div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
    						<div class="video-card">
    						<a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                                <div class="player">
                                  <p class="event-name"></p>
                                  <p class="event-category"></p>
                                  <video id="player1" class="video_class_detail" width="640" height="360" preload="none" style="width: 100%; max-width: 100%; height: 280px" controls poster="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.thumbnail+`" playsinline webkit-playsinline>
                                     <source src="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.file+`" type="video/mp4">
                                  </video>
                                </div>
                                </a>
                                	</div></div>
                            </div></div></div>`;
        return a;
    }
    function imgHtml(item){
    	var a = `<div class="col-md-4" style="float:left">

    				<div class="post-img-grid border-0 pb-0 mb-0">
                              <div class="img-posted pb-0 border-0">
                                <div class="item dfbkdjsf">
                                  <a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                                  <div class="img-card-parent">
                                    <div class="abc"></div>
                                     <p class="event-name"></p>
                                              <p class="event-category"></p>
                                    <div class="img-card">
                                            <img src="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.file+`" class="img-responsive " />
                                    </div>
                                  </div>
                                  </a>
                                </div>
                              </div>
                            </div></div>`;
        return a;
    }
    function audioHtml(item,key){
    	var a = `<div class="col-md-4" style="float:left"><div class="post-audio-grid border-0 pb-0 mb-0">
                  <div class="audio-posted pb-0 border-0">
                      <div class="audio-owl-slider  owl-theme ">
                      	<a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                          <div id="waveform_`+key+`" class="audio-waveform wave" style="height:200px">
                              <p class="event-name"></p>
                              <p class="event-category"></p>

                              <img src="https://storage.googleapis.com/gong_bucket/uploads/image/1585216412audio.PNG" class="img-responsive img_`+key+`" style="position: absolute;overflow: hidden; margin-top: 54px;margin-left: 19px;" />
                          </div>
                          </a>
                          <div class="">

                          </div>
                      </div>
                  </div>
              </div></div>`;
        return a;
    }

    function renderWaveForm(url, parentSelector) {
      var domEl = document.createElement('div')
      document.querySelector(parentSelector).appendChild(domEl)

      var wavesurferr = WaveSurfer.create({
        container: domEl,
        waveColor: 'red',
        progressColor: 'purple',
        hideScrollbar: true,
        xhr: {
        cache: "default",
        mode: "cors",
        method: "GET",
        // credentials: "gong_bucket",
        headers: [
          { key: "cache-control", value: "no-cache" },
          { key: "Access-Control-Allow-Origin", value: "*" },
          { key: "Origin", value: "http://localhost:8000" },
          { key: "Origin", value: "*" },
          { key: "pragma", value: "no-cache" }
        ]
      }
      });
      wavesurferr.load(url);
      return wavesurferr;
    }
</script>
@endsection

@section('css')
@parent 
	 <link href="{{asset('assets/css/flipdown.css')}}" rel="stylesheet" type="text/css" />
  
@endsection
@section('script')
@parent
	<script src="{{asset('assets/js/flipdown.js')}}" type="text/javascript"></script>
@endsection
<div class="col-lg-3 col-sm-12">
    <div class="user-card">
        <img src="{{config('constant.BUCKET_URL') . userProfilePhoto(Auth::user()) }}" alt="">
        <div>
            <label for="">{{userName(Auth::user())}}</label>
        </div>
        <p>{{userTier(Auth::user())}}</p>
        <p>@if(Auth::user()->hasRole('user'))
                Talent
            @elseif(Auth::user()->hasRole('fan'))
                Fan
            @elseif(Auth::user()->hasRole('agent'))
                Agent
            @elseif(Auth::user()->hasRole('sponsor'))
                Sponsor
            @endif
        </p>
    </div>
    <div class="d-flex flex-column flex-lg-column justify-content-sm-between justify-content-center flex-wrap">
        <div class="user-info">
            <div class="info-field" data-toggle="tooltip" data-placement="top" title="{{userName(Auth::user())}}">
                <span><i class="fa fa-hand-scissors-o"></i>Search me </span> <label>{{userName(Auth::user())}}</label>
            </div>
{{--        {{$data['user']['email']}}--}}
{{--        {{$data['user']['email']}}--}}
           <!--  <div class="info-field" data-toggle="tooltip" data-placement="top" title="email">
                <span><i class="fa fa-envelope-o"></i>Email </span> <a href="mailto:johndoe@company.com" > email</a>

            </div> -->
           <!--  <div class="info-field">
{{--           {{$data['user']['phone']}}--}}
                <span><i class="fa fa-phone"></i>Reach me at  </span> <label>PHONE</label>
            </div> -->
            <div class="info-field">
                <span><i class="fa fa-venus"></i>Gender is</span> <label class="text-capitalize"> {{gender(Auth::user())}}</label>
            </div>
            @if(Auth::user()->hasRole('user') || Auth::user()->hasRole('fan'))
                <div class="info-field">
                    <span><i class="fa fa-calendar"></i>Wish me on  </span> <label>
                    {{date(config('constant.date_format'),strtotime(dateOfBirth(Auth::user())))}}</label>
                </div>
            @endif
            <div class="info-field">
                <span><i class="fa fa-calendar"></i>Joined on  </span>
                <label>
                {{date(config('constant.date_format'),strtotime(Auth::user()->created_at))}}</label>
            </div>
            <div class="info-field">
                <span><i class="fa fa-home"></i>Lives in&nbsp;</span> <label> {{region(Auth::user())}}, &nbsp;</label>
                <label>{{country(Auth::user())}}</label>
            </div>

        </div>
        <div class="competition-btn-group">
            @if(Auth::user()->hasRole([1]))
                <div class=" py-3 text-center">
                    <button class="btn btn-primary rounded-pill btn_submit_competition_sidebar">Compete</button>
                </div>

            @endif
        <!--  <div class=" py-3 text-center">
                 <a class="btn btn-primary rounded-pill" href="{{route('user.competition.private_join')}}">Join Private Network</a>
             </div> -->

            @if(Auth::user()->hasRole([1,3,4,5]))
                <div class=" py-3 justify-content-center mb-4 nav nav-tabs"  role="tablist">
                    <a class="btn btn-primary rounded-pill" href="{{route('user.competition.create')}}">Create Competition</a>
                </div>
            @endif
        </div>
    </div>
</div>

@section('script')
@parent
    <script type="text/javascript">
        $('.btn_submit_competition_sidebar').click(function(){

           window.location.href = "{{route('user.dashboard')}}?is_compete=1";
            // $('.nav_check_class').removeClass('active');
            // $("#nav-Social").hide();
            // $("#nav-Competitions").hide();
            // $('.competition_tab_submit_for').show();
        });
        $("#nav-Competitions-tab").click(function(){
            $('.competition_tab_submit_for').hide();
            $('#nav-Competitions').show();
            $('#nav-Social').hide();
        });
        $("#nav-Social-tab").click(function(){
            $('.competition_tab_submit_for').hide();
            $('#nav-Social').show();
            $('#nav-Competitions').hide();
        });
    </script>
@endsection

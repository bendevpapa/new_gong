@extends('layouts.master')
@section('content')
    <div class="container mt-5">
        <div class="row">
            @include('frontend.users.layout.left-sidebar')
            <div class="col-lg-9 col-sm-12">
                @yield('dash-content')
            </div>
        </div>
    </div>
@endsection

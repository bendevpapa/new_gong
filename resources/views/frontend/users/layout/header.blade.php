<head>
    <meta charset="UTF-8">
    <title>Gong</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--  Font Icons-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
    {{-- <link href="{{asset('assets/css/jquery.waitablebutton.css')}}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{asset('assets/css/video/mediaelementplayer.min.css')}}">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.15/mediaelementplayer-legacy.min.css" integrity="sha256-t5CKAVpWfsI2MBHfJHU2jb/zQ2Dp2j/f9QYE1jlftkY=" crossorigin="anonymous" />
    <!--Bootstrap 4 Css -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!--Custom Style Sheets-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="{{asset('assets/css/theme-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/modals.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom2.css')}}">
    <style>
        .tt-suggestion.tt-selectable {
            border-bottom: 1px solid #6d6d6d;
            margin-bottom: 10px;
            padding-bottom: 10px;
            cursor: pointer;
        }
        .tt-menu.tt-open {
            background: #FEC157;
            width: 100%;
            border-radius: 21px;
            padding: 10px 20px;
        }
        .tt-suggestion.tt-selectable:hover{
            opacity: 0.7;
        }
        .remove_fields{
            display: none;
        }
        .radio-inline{
            margin-right: 35px;
        }
        #nav-comment h1.text-muted.my-5 {
            float: left;margin-bottom: 20px !important;
        }
        button#post_comment_btn {
            border-radius: 50rem!important;
            float: left !important;
            margin: 20px 0;
        }
    </style>
    @yield('css')
</head>

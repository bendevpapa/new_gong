<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Gong</title>
    <meta name="description" content="The HTML5 Herald">
    <meta name="author" content="SitePoint">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

</head>

<body>

    <div class="container pt-5">
        <div class="card" 	>
		  <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
		  <div class="card-body">
		    <h5 class="card-title">Competition </h5>
		    <table class="table">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Competition id</th>
			      <th scope="col">Competition Name</th>
			      <th scope="col">Competition type</th>
			      <th scope="col">auto/custom</th>
			      <th scope="col">Competition member</th>
			      <th scope="col">parent ID</th>
			      <th scope="col">No of Competitor(require)</th>
			      <th>winner Email </th>
			      <!-- <th scope="col">No of Rounds(require)</th> -->
			      <th scope="col">Competition Status</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@foreach($competition as $key=>$value)
			    <tr>
			      <th scope="row">1</th>
			      <td>{{$value->id}}</td>
			      <td>{{$value->competition_name}}</td>
			      <td>{{$value->competition_type}}</td>
			      <td>{{$value->comopetition_by_type }}</td>
			      <td>{{count($value->activeCompetitionDetail)}}</td>
			      <td>{{$value->parent_id}}</td>
			      <td>{{$value->no_of_competitiors}}</td>
			      <td>{{($value->winnerCompetitionDetail) ? $value->winnerCompetitionDetail->user->email : ''}}</td>
			      <!-- <td>{{$value->no_of_rounds}}</td> -->
			      <td>
			      	@if($value->is_active == 0)
			      		Off
			      	@elseif($value->is_active == 1)
			      		Active
			      	@elseif($value->is_active == 3)
			      		waiting for comepetitiors
			      	@elseif($value->is_active  == 5)
			      		Hold
			      	@endif

			      </td>
			    </tr>
			    @endforeach
			  </tbody>
			</table>
			{{ $competition->links() }}
		  </div>
		</div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>

    

</body>
</html>
